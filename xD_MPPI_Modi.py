import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cm
import numpy as np
from numpy.linalg import inv
import random
import quadcopter as QUAD
import gui as GUI
from parameters import Env_Parameters

class MPPI():
    def __init__(self, Env, Parameters):

        self.Parameters = Parameters
        self.Env = Env
        self.Flag_GUI = Parameters['Flag_GUI']

        if self.Env == 'Quadcopter':
            print('environment_quad')
            from environment_quad import Environment
            self.env = Environment(self.Parameters)

        elif Env in ['DoubleSlit1D', 'DoubleSlit2D']:
            print('environment_double_slit')
            from environment_double_slit import Environment
            self.env = Environment(self.Env, self.Parameters)

        ###############################################################################
        self.INF = np.inf
        self.N = self.Parameters['N'] # Number of timesteps of sampling
        self.K = self.Parameters['K'] # Number of samples
        self.NOISE_COVARIANCE = self.Parameters['Noise_Cov']
        if type(self.NOISE_COVARIANCE) == type(np.array([])):
            self.NOISE_COVARIANCE_MATRIX = self.NOISE_COVARIANCE
            self.NOISE_COVARIANCE = self.NOISE_COVARIANCE[0]
        else:
            self.NOISE_COVARIANCE_MATRIX = np.ones(self.env.CONTROL_DIM) * self.NOISE_COVARIANCE
        self.CONTROL_COST = self.Parameters['Control_Cost']
        self.TEMPERATURE = self.NOISE_COVARIANCE * self.CONTROL_COST
        self.CONTROL_COST_MATRIX = np.diag(np.ones(self.env.CONTROL_DIM)) * self.CONTROL_COST
        # Parameter for normal MPPI
        self.PARA_DELTA_U = 0.1/np.sqrt(self.env.dt)

        ###############################################################################
        self.STATE_INIT = self.env.STATE_INIT
        self.CONTROL_INIT = np.zeros(self.env.CONTROL_DIM)

        self.state = self.STATE_INIT
        # state_sequence can be used to plot the trajectory
        self.state_sequence = np.array([np.full(self.env.STATE_DIM, None) for _ in range(self.N)])
        self.control_sequence = np.zeros((self.N, self.env.CONTROL_DIM))

        ###################################################################################
        # Initial the matrix to save the path cost & control noise
        self.cost_matrix = np.ones((self.K, self.N))*self.INF
        self.cost_matrix[:, 0] = 0
        self.delta_u_matrix = np.zeros((self.K, self.N, self.env.CONTROL_DIM))

        # Parameters of Information Theory MPPI
        self.cost_total = np.zeros(shape=(self.K))

        # Generate random control variations (delta_u)
        self.control_noise_all = np.random.normal(0, np.ones((self.K, self.N, self.env.CONTROL_DIM)) * self.NOISE_COVARIANCE_MATRIX)
        self.state_sequence_all = np.array([np.array([np.full(self.env.STATE_DIM, None) for _ in range(self.N)]) for _ in range(self.K)])

    # Smoothing the sampling trajectory
    def smooth(self, y, box_pts):
        box = np.ones(box_pts)/box_pts
        y_smooth = np.convolve(y, box, mode='same')
        return y_smooth

    def sampling_trajectories(self):
        # Initial the matrix to save the path cost & control noise
        self.cost_matrix = np.ones((self.K, self.N))*self.INF

        cost_end, self.cost_matrix[:, 0], _ = self.env.cost_path_calculate(self.STATE_INIT, np.zeros(self.env.CONTROL_DIM))

        self.delta_u_matrix = np.zeros((self.K, self.N, self.env.CONTROL_DIM))
        self.cost_total = np.ones(shape=(self.K)) * self.cost_matrix[0,0]

        # Number of failed sampling trajectories
        num_inf = []

        # Generate random control variations (delta_u)
        self.control_noise_all = np.random.normal(0, np.ones((self.K, self.N, self.env.CONTROL_DIM)) * self.NOISE_COVARIANCE_MATRIX)
        self.state_sequence_all = np.array([np.array([np.full(self.env.STATE_DIM, None) for _ in range(self.N)]) for _ in range(self.K)])
        # Sampling K trajectories
        for k in range(self.K):
            control_noise = self.control_noise_all[k]
            # Smoothing the random sampling trajectories
            for dim in range(self.env.CONTROL_DIM):
                control_noise[:,dim] = self.smooth(control_noise[:,dim], 5)
            self.state_sequence = np.array([np.full(self.env.STATE_DIM, None) for _ in range(self.N)])
            self.state = self.STATE_INIT
            self.state_sequence[0] = self.STATE_INIT
            Flag_Collision = False
            # Sampling one trajectory
            for i in range(self.N-1):

                self.state, cost_end, cost_path, Flag_Collision = self.env.step(self.state, self.control_sequence[i], control_noise[i])

                # Calculate delta_u
                delta_u =  self.PARA_DELTA_U * control_noise[i]
                # Cost of importance sampling
                if self.Parameters['Algorithm'] == 'MPPI':
                    # For Normal MPPI
                    if self.Parameters['Flag_CovarianceVariable']:
                        # cost of likelihood ratio term
                        cost_path = cost_path + \
                                    (1-1/self.NOISE_COVARIANCE)/2 * delta_u.dot(self.CONTROL_COST_MATRIX).dot(delta_u) + \
                                    self.control_sequence[i].dot(self.CONTROL_COST_MATRIX).dot(delta_u) + \
                                    0.5 * self.control_sequence[i].dot(self.CONTROL_COST_MATRIX).dot(self.control_sequence[i]) #+ \
                                    #0.5 * (self.state - self.env.GOAL).dot(self.state - self.env.GOAL) + \
                    else:
                        cost_path = cost_path + \
                                    self.control_sequence[i].dot(self.CONTROL_COST_MATRIX).dot(delta_u) + \
                                    0.5 * self.control_sequence[i].dot(self.CONTROL_COST_MATRIX).dot(self.control_sequence[i]) #+ \
                                    #0.5 * (self.state - self.env.GOAL).dot(self.state - self.env.GOAL) + \
                else:
                    # For Info MPPI
                    # TODO: inv(Sigma) should be a matrix
                    cost_path = cost_path
                    cost_path = cost_path + \
                                0.5*self.TEMPERATURE * self.control_sequence[i].dot(1/self.NOISE_COVARIANCE).dot(self.control_sequence[i] + 2*control_noise[i])# + 2*control_noise[i])
                    #            0.5*self.TEMPERATURE * self.control_sequence[i].dot(inv(np.diag(np.ones(self.env.CONTROL_DIM)) * self.NOISE_COVARIANCE)).dot(self.control_sequence[i] + 2*control_noise[i])

                # Save the cost in matrix
                self.cost_matrix[k, i+1] = self.cost_matrix[k, i] + cost_path
                #self.cost_matrix[k, i+1] = cost_path

                # Save the control noise in matrix
                if self.Parameters['Algorithm'] == 'MPPI':
                    self.delta_u_matrix[k, i] = delta_u
                else:
                    self.delta_u_matrix[k, i] = control_noise[i]
                # Save the state in matrix
                self.state_sequence[i+1] = self.state

                # INFO - MPPI
                self.cost_total[k] += cost_path

                # Plus the end cost
                #print('cost_end: {}'.format(cost_end))
                self.cost_matrix[k, :] += cost_end
                self.cost_total[k] += cost_end

                # Rendering every step of sampling
                if False:
                    self.env.render('q1', self.state[0:3], self.state[6:9])

                # Check if collision
                if Flag_Collision:
                    break

            self.state_sequence_all[k] = self.state_sequence

            if self.cost_matrix[k, -1] == np.inf:
                num_inf += [k]

        # Delete the samplings, which failed
        self.cost_matrix = np.delete(self.cost_matrix, num_inf, axis=0)
        self.cost_total = np.delete(self.cost_total, num_inf, axis=0)
        self.delta_u_matrix = np.delete(self.delta_u_matrix, num_inf, axis=0)
        print('Length of successful sampling: {}'.format(len(self.cost_total)))
        if len(self.cost_total) > 0:
            print('Cost Total -- max:{}, min:{}'.format(max(self.cost_total), min(self.cost_total)))

        if False and self.Flag_GUI and len(self.cost_total) != 0:
            #print(state_sequence_all)
            self.state_sequence_all = np.delete(self.state_sequence_all, num_inf, axis=0)
            coolwarm = plt.get_cmap('coolwarm')
            my_norm = colors.Normalize((0.0), (30000.0))
            map = cm.ScalarMappable(norm=my_norm, cmap='coolwarm')

            for i in range(len(self.cost_total)):
                my_col = map.to_rgba(self.cost_total[i])
                if self.Env == 'Quadcopter':
                    self.env.gui.ax.plot(self.state_sequence_all[i, :, 0], self.state_sequence_all[i, :, 1], self.state_sequence_all[i, :, 2], alpha = 0.1, c=my_col)#self.cost_total[i], cmap=CM.jet)
                else:
                    self.env.ax0.plot(self.state_sequence_all[i, :, 0], self.state_sequence_all[i, :, 1], alpha = 0.1, c=my_col)
            plt.pause(0.000001)

    def control_update(self):
        # Update the control sequence from all the sampling trajectories
        #print('Lenght of cost_total: ', len(self.cost_total), ', Noise Covariance: ', self.NOISE_COVARIANCE)

        if len(self.cost_matrix) > 0:
            if self.Parameters['Algorithm'] == 'MPPI':
                temp_cost = self.cost_matrix[:, -1]
            else:
                temp_cost = self.cost_total
            print('Different Cost in Update Law: {} and {}'.format(self.cost_matrix[:5,-1], self.cost_total[:5]))

            if True:
                # Update Law for Info MPPI
                if True:
                    # For Environment Quadcopter
                    # Normalization
                    cost_total_temp = np.exp(-1/self.TEMPERATURE*(temp_cost - np.min(temp_cost)))# - np.min(self.cost_total)
                else:
                    # For Environment Double Slit
                    cost_matrix_temp = np.exp(-1/self.TEMPERATURE*(temp_cost))
                omega = cost_total_temp / np.sum(cost_total_temp)
                self.control_sequence = self.control_sequence + [np.sum(np.multiply(omega, self.delta_u_matrix[:, t].T), axis=1) for t in range(self.N)]
            else:
                # Update Law for normal MPPI
                for i in range(self.N):
                    # TODO: calculate -1 or i in cost_matrix ???
                    if True:
                        # For Environment Quadcopter
                        cost_matrix_temp = np.exp(-1/self.TEMPERATURE*(temp_cost - np.min(temp_cost)))
                    else:
                        # For Environment Double Slit
                        cost_matrix_temp = np.exp(-1/self.TEMPERATURE*(temp_cost))
                    sum_nominator = cost_matrix_temp.dot(self.delta_u_matrix[:, i])#+self.control_sequence[i]))
                    sum_denominator = np.sum(cost_matrix_temp)

                    if sum_denominator != 0:
                        self.control_sequence[i] += np.array(sum_nominator/sum_denominator)
                    if i == 0:
                        print('Control Delta: {}'.format(np.array(sum_nominator/sum_denominator)))
                        print('Sum_nominator/de: {}, {}'.format(sum_nominator, sum_denominator))
            return True
        else:
            #print('Length of cost_matrix is 0')
            #self.control_sequence = np.zeros((self.N, self.env.CONTROL_DIM))
            return False

    def sequence_update(self):
        # Update Control Sequence
        control_output = self.control_sequence[0]
        sequence = self.control_sequence
        self.control_sequence = np.roll(self.control_sequence, -self.env.CONTROL_DIM)
        self.control_sequence[-1] = self.CONTROL_INIT
        # Update the init state of the next epoch
        self.STATE_INIT, _, _, _ = self.env.step(self.STATE_INIT, control_output, 0.0)
        # Update the new state in the environment
        self.env.state = self.STATE_INIT
        return control_output, sequence, self.STATE_INIT

if __name__ == "__main__":
    Env = 'DoubleSlit1D'#'Quadcopter'#
    mppi = MPPI(Env, Env_Parameters[Env])
    RUN = True
    num = 0
    control_output_sequence = np.array([np.zeros(mppi.env.CONTROL_DIM)])
    if Env == 'Quadcopter':
        mppi.env.render('q1', mppi.env.STATE_INIT[0:3], mppi.env.STATE_INIT[6:9])
    else:
        mppi.env.render_init()

    # Test Sampling Trajectory
    #while RUN:
    #    mppi.sampling_trajectories()

    while RUN:
        num += 1
        mppi.sampling_trajectories()
        mppi.control_update()
        control_output, control_sequence, state_init = mppi.sequence_update()

        # Plot the control sequence every epoch
        if True:
            coolwarm = plt.get_cmap('coolwarm')
            my_norm = colors.Normalize(0, 1000)
            map = cm.ScalarMappable(norm=my_norm, cmap='coolwarm')

            x = np.array([np.zeros(mppi.env.STATE_DIM) for _ in range(mppi.N)])
            # There is problem, because x[0] should be one step before the state_init
            x[0] = state_init
            for i in range(len(control_sequence)-1):
                x[i+1], _, _, _ = mppi.env.step(x[i], control_sequence[i], np.zeros(mppi.env.CONTROL_DIM))
                #mppi.env.render('q1', x[i+1][0:3], x[i+1][6:9])
            if Env == 'Quadcopter':
                mppi.env.plot_trajectory(x[:, 0:3], 'r')
            else:
                _, temp_cost, _= mppi.env.cost_path_calculate(x[0], control_output)
                my_col = map.to_rgba(np.clip(temp_cost, 0, 1000))
                mppi.env.ax1.plot(x[:,0], x[:,1], alpha = 0.1, c = my_col)
                print(num, temp_cost)

        # Update the rendering of quadcopter
        if Env == 'Quadcopter':
            if mppi.env.check_collision(state_init):
                mppi.env.gui.color_ring = 'r'
            v = np.sqrt(state_init[3]**2 + state_init[4]**2 + state_init[5]**2)
            distance = np.sqrt((state_init[:3] - mppi.env.GOAL[:3]).dot(state_init[:3] - mppi.env.GOAL[:3]))
            print(' Num: {} \n Control Output: {} \n State Init: {} \n Path Cost: {} \n Velocity: {}, Distance: {}'.format(num, control_output, state_init, mppi.env.cost_path_calculate(state_init, control_output), v, distance))
            #print(' Seperate Cost: {}, {}, {}, {}'.format(200*((state_init[0]/(2.0/2))**2+(state_init[1]/(2.0/2))**2-1)**2, 20*(state_init[2]-2)**2, 20*(v - 1.5)**2, 0.5*(state_init[3:]).dot(state_init[3:])))
            mppi.env.render('q1', state_init[0:3], state_init[6:9])

        flag_finished, state_sequence, control_sequence, cost_sequence = mppi.env.model_update(control_output)
        if flag_finished:
            if Env != 'Quadcopter':
                mppi.env.plot_trajectory(2, state_sequence[:,0], state_sequence[:,1])
            # Initial the values for the next epoch
            mppi.STATE_INIT = mppi.env.STATE_INIT
            mppi.state_sequence = np.array([np.full(mppi.env.STATE_DIM, None) for _ in range(mppi.N)])
            mppi.control_sequence = np.zeros((mppi.N, mppi.env.CONTROL_DIM))
            print(num)
            print(flag_finished, state_sequence)
            #if num >= 100:
                #if mppi.env.check_finished(state_sequence[-1]):
                #    print('Finished')
                #else:
                #    print(state_sequence[-1])
