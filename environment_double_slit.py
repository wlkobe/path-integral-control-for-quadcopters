import matplotlib.pyplot as plt
import numpy as np
import random
from parameters import Env_Parameters

class Environment():
    def __init__(self, Environment, Parameters):

        if Environment == 'DoubleSlit1D':
            self.FLAG_1D = True
        else:
            self.FLAG_1D = False
        self.STATE_DIM = Parameters['State_Dim']
        self.CONTROL_DIM = Parameters['Control_Dim']
        self.INF = np.inf

        if self.FLAG_1D:
            self.STATE_INIT = np.array([0.0, 0.0])  #[time, state]
            self.GOAL = np.array([2.0, 0.0])        #[time, state]
        else:
            self.STATE_INIT = np.array([-8.0, 0.0])  #[time, state]
            self.GOAL = np.array([5,-3])#[8.0, -7.0])        #[time, state]

        self.time = 0   # Current Time
        self.dt = 0.02
        self.state = self.STATE_INIT  # Current State
        self.counter = 0
        if self.FLAG_1D:
            self.M = Parameters['M']   # Number of timesteps of process
        else:
            self.M = Parameters['M']
        self.BOUND = 20

        self.CONTROL_COST = 0.0002
        self.CONTROL_COST_MATRIX = np.diag(np.ones(self.CONTROL_DIM)) * self.CONTROL_COST

        self.time_sequence = np.linspace(0, 2, self.M)
        self.state_sequence = np.array([np.full(self.STATE_DIM, None) for _ in range(self.M)])
        self.control_sequence = np.array([np.full(self.CONTROL_DIM, None) for _ in range(self.M)])
        self.cost_sequence = np.ones(self.M)*np.inf
        self.state_sequence[0] = self.state
        self.control_sequence[0] = np.zeros(self.CONTROL_DIM)
        _, self.cost_sequence[0], _ = self.cost_path_calculate(self.state, np.zeros(self.CONTROL_DIM))

        # Initital the figure
        self.fig0 = plt.figure(1, figsize = (9,3))
        #self.fig1 = plt.figure(2, figsize = (6, 6))

        self.ax0 = self.fig0.add_subplot(131)
        #self.ax0.set_title('Double Slit')
        #self.ax0.set_xlim((0, 2))
        self.ax0.set_ylim((-10, 10))
        self.ax0.set_xlabel('x1')
        self.ax0.set_ylabel('x2')
        self.ax1 = self.fig0.add_subplot(132)
        self.ax1.set_title('Double Slit')
        #self.ax1.set_xlim((0, 2))
        self.ax1.set_ylim((-10, 10))
        self.ax1.set_xlabel('x1')
        #self.ax1.set_ylabel('K')
        self.ax2 = self.fig0.add_subplot(133)
        #self.ax2.set_title('Cost Matrix')
        #self.ax2.set_xlim((0, 2))
        self.ax2.set_ylim((-10, 10))
        self.ax2.set_xlabel('x1')
        #self.ax2.set_ylabel('K')
        if self.FLAG_1D:
            self.ax0.set_xlim((0, 2))
            self.ax1.set_xlim((0, 2))
            self.ax2.set_xlim((0, 2))
        else:
            self.ax0.set_xlim((-10, 10))
            self.ax1.set_xlim((-10, 10))
            self.ax2.set_xlim((-10, 10))
        """
        self.bx0 = self.fig1.add_subplot(211)
        self.bx0.set_title('Control Sequence Final')
        self.bx0.set_ylim((-25, 25))
        self.bx0.set_xlim((0, self.M*self.dt))
        self.bx0.set_xlabel('t')
        self.bx0.set_ylabel('N')
        self.bx0.grid(True)
        self.bx1 = self.fig1.add_subplot(212)
        self.bx1.set_title('Control Sequence Step')
        self.bx1.set_ylim((-25, 25))
        self.bx1.set_xlim((0, self.M*self.dt))
        self.bx1.set_xlabel('t')
        self.bx1.set_ylabel('N')
        self.bx1.grid(True)
        """
    # Particle Dynamic Equation
    def particle_dynamic(self, u, delta_u):
        if type(delta_u) != type(np.array([None, None])):
            delta_u = np.array([delta_u, delta_u])
        try:
            u = np.array(u) + np.array(delta_u)
        except:
            print(u, delta_u)
        #u = np.clip(u, a_min = -self.BOUND, a_max = self.BOUND )
        if self.FLAG_1D:
            u[0] = 1.0  #time
        return u * self.dt

    # Check if the particle collision
    def check_collision(self, state):
        Flag_Collision = False
        if self.FLAG_1D:
            # Check if the particle aut of range
            if state[1] < -10 or state[1] > 10:
                Flag_Collision = True
            # Check if the particle collision the wall
            if (state[0] >= 0.97 and state[0] <= 1.02) and ((state[1] < -6) or (-4 < state[1] < 6) or (state[1] > 8)):
                Flag_Collision = True
        else:
            # Check if the particle aut of range
            if state[0] < -10.0 or state[0] > 10.0 or state[1] < -10.0 or state[1] > 10.0:
                Flag_Collision = True
            # Check if the particle collision the wall
            if (state[0] >= -0.8 and state[0] <= 0.8) and ((state[1] < -6.0) or (-4.0 < state[1] < 6.0) or (state[1] > 8.0)):
                Flag_Collision = True
        return Flag_Collision


    # Calculate the path cost function
    def cost_path_calculate(self, state, control_output):
        # Initial the cost of one trajectory
        cost_path = 0
        # Initial the flag of collision
        Flag_Collision = self.check_collision(state)
        # Check if the particle collision the wall or out of range
        if Flag_Collision:
            cost_path = self.INF

        # cost of likelihood ratio term
        #cost_path = cost_path + \
        #            0.5 * control_output.dot(self.CONTROL_COST_MATRIX).dot(control_output) #+ \
        #            (1-1/self.NOISE_COVARIANCE)/2 * delta_u.dot(self.CONTROL_COST_MATRIX).dot(delta_u) + \
        #            control_output.dot(self.CONTROL_COST_MATRIX).dot(delta_u) + \
                    #0.5 * (self.state - self.env.GOAL).dot(self.state - self.env.GOAL) + \

        cost_end = 0
        if self.FLAG_1D:
            if state[0] >= 1.9 and state[0] <= 2.1:
                cost_end = 0.5 * (state[1] - self.GOAL[1])**2
                #cost_end = 0.5 * (state[1] - self.GOAL[1]).dot(state[1] - self.GOAL[1])
                #cost_path += cost_end
        else:
            distance = np.sqrt((state - self.GOAL).dot(state - self.GOAL))
            cost_path += 0.5 * (state - self.GOAL).dot(state - self.GOAL)
            #if distance < 0.5:
            #    cost_end = -5000
        #return cost_path, Flag_Collision
        return cost_end, cost_path, Flag_Collision

    def step(self, state, control, noise):
        state = state + self.particle_dynamic(control, noise)
        #cost_path, Flag_Collision = self.cost_path_calculate(state, control)
        cost_end, cost_path, Flag_Collision = self.cost_path_calculate(state, control+noise)
        #return state, cost_path, Flag_Collision
        return state, cost_end, cost_path, Flag_Collision

    def check_success(self):
        distance = np.sqrt((self.state - self.GOAL).dot(self.state - self.GOAL))
        if distance < 1.0:
            Flag_Success = True
        else:
            Flag_Success = False
        return Flag_Success

    # Update the state in one step
    def model_update(self, u):
        temp_state_sequence = np.array([np.full(self.STATE_DIM, None) for _ in range(self.M)])
        temp_control_sequence = np.array([np.full(self.CONTROL_DIM, None) for _ in range(self.M)])
        temp_cost_sequence = np.ones(self.M)*np.inf
        Flag_Success = False
        # Check if time over range
        if (self.time >= 0) and (self.counter < self.M-1):
            self.time += 1*self.dt
            self.counter += 1
            Flag_Loop = False
        # If time over range, update a new loop
        else:
            # TODO: Control sequence is always new policy in the next epoch.
            Flag_Success = self.check_success()
            self.state = self.STATE_INIT
            self.time = (0 + 1)*self.dt
            self.counter = 1
            temp_state_sequence = self.state_sequence
            temp_control_sequence = self.control_sequence
            temp_cost_sequence = self.cost_sequence
            self.cost_sequence = np.ones(self.M)*np.inf
            self.control_sequence = np.array([np.full(self.CONTROL_DIM, None) for _ in range(self.M)])
            self.state_sequence = np.array([np.full(self.STATE_DIM, None) for _ in range(self.M)])
            self.state_sequence[0] = self.state
            self.control_sequence[0] = np.zeros(self.CONTROL_DIM)
            _, self.cost_sequence[0], _ = self.cost_path_calculate(self.state, np.zeros(self.CONTROL_DIM))
            Flag_Loop = True
        # If collision, update a new loop
        if self.check_collision(self.state):
            Flag_Success = self.check_success()
            self.state = self.STATE_INIT
            self.time = (0 + 1)*self.dt
            self.counter = 1
            temp_state_sequence = self.state_sequence
            temp_control_sequence = self.control_sequence
            temp_cost_sequence = self.cost_sequence
            self.cost_sequence = np.ones(self.M)*np.inf
            self.control_sequence = np.array([np.full(self.CONTROL_DIM, None) for _ in range(self.M)])
            self.state_sequence = np.array([np.full(self.STATE_DIM, None) for _ in range(self.M)])
            self.state_sequence[0] = self.state
            self.control_sequence[0] = np.zeros(self.CONTROL_DIM)
            _, self.cost_sequence[0], _ = self.cost_path_calculate(self.state, np.zeros(self.CONTROL_DIM))
            Flag_Loop = True
        else:
            self.state = self.state + self.particle_dynamic(u, 0)
            self.state_sequence[self.counter] = self.state
            self.control_sequence[self.counter] = u
            _, self.cost_sequence[self.counter], _ = self.cost_path_calculate(self.state, u)
            #self.state_sequence[int(self.time/self.dt)] = self.state
        return Flag_Loop, temp_state_sequence, temp_control_sequence, temp_cost_sequence, Flag_Success

    def check_finished(self, state):
        if None not in state:
            distance = np.sqrt((state-self.GOAL).dot(state-self.GOAL))
            if distance < 1 and state[0] > 1.8:
                return True
            else:
                return False
        return False

    # Plot the environment
    def render_init(self):
        if self.FLAG_1D:
            mid = 1.0
        else:
            mid = 0.0
        self.ax0.axvline(x=mid, ymin=0.9, ymax=1, linewidth=3, color='k')
        self.ax0.axvline(x=mid, ymin=0.3, ymax=0.8, linewidth=3, color='k')
        self.ax0.axvline(x=mid, ymin=0, ymax=0.2, linewidth=3, color='k')
        self.ax1.axvline(x=mid, ymin=0.9, ymax=1, linewidth=3, color='k')
        self.ax1.axvline(x=mid, ymin=0.3, ymax=0.8, linewidth=3, color='k')
        self.ax1.axvline(x=mid, ymin=0, ymax=0.2, linewidth=3, color='k')
        self.ax2.axvline(x=mid, ymin=0.9, ymax=1, linewidth=3, color='k')
        self.ax2.axvline(x=mid, ymin=0.3, ymax=0.8, linewidth=3, color='k')
        self.ax2.axvline(x=mid, ymin=0, ymax=0.2, linewidth=3, color='k')
        plt.pause(0.0000001)

    # Plot the result
    def plot_trajectory(self, key, x, y):
        if key == 0:
            self.ax0.plot(x, y)
        elif key == 1:
            self.ax1.plot(x, y)
        elif key == 2:
            self.ax2.plot(x, y)
        plt.pause(0.0000000001)

    def plot_sequence(self, key, data):
        if key == 0:
            #self.bx0.cla()
            # Plot the sequences
            for i in range(self.CONTROL_DIM):
                tf = np.linspace(0, len(data[1:, i])*self.dt, len(data[1:, i]))
                self.bx1.plot(tf, data[1:, i], label = str(i))
            self.bx1.set_title('Control Sequence Final')
            self.bx1.set_ylim((-25, 25))
            self.bx1.set_xlim((0, self.M*self.dt))
            self.bx1.set_xlabel('t')
            self.bx1.set_ylabel('N')
            self.bx1.legend(loc = 1)
            self.bx1.grid(True)
        elif key == 1:
            self.bx1.cla()
            # Plot the sequences
            for i in range(self.CONTROL_DIM):
                tf = np.linspace(self.time, self.time+len(data[:, i])*self.dt, len(data[:, i]))
                self.bx1.plot(tf, data[:, i], label = str(i))
            self.bx1.set_title('Control Sequence Step')
            self.bx1.set_ylim((-25, 25))
            self.bx1.set_xlim((0, self.M*self.dt))
            self.bx1.set_xlabel('t')
            self.bx1.set_ylabel('N')
            self.bx1.legend(loc = 1)
            self.bx1.grid(True)
        plt.pause(0.0000000001)

# Smoothing the sampling trajectory
def smooth(y, box_pts):
    box = np.ones(box_pts)/box_pts
    y_smooth = np.convolve(y, box, mode='same')
    return y_smooth

if __name__ == '__main__' :
    Env = 'DoubleSlit2D'
    env = Environment(Env, Env_Parameters[Env])
    font_size = 14

    fig0 = plt.figure(3, figsize = (4,4))

    ax0 = fig0.add_subplot(111)
    ax0.set_title('Double Slit 2D', size = font_size*1.5)
    ax0.set_xlim((-10, 10))
    ax0.set_ylim((-10, 10))
    ax0.set_xlabel('x1', size = font_size)
    ax0.set_ylabel('x2', size = font_size)
    mid = 0.0
    ax0.axvline(x=mid, ymin=0.9, ymax=1, linewidth=3, color='k')
    ax0.axvline(x=mid, ymin=0.3, ymax=0.8, linewidth=3, color='k')
    ax0.axvline(x=mid, ymin=0, ymax=0.2, linewidth=3, color='k')

    noise = np.random.normal(0, 50*np.ones((100,2)))
    noise[:,0] = smooth(noise[:,0], 10)
    noise[:,1] = smooth(noise[:,1], 10)
    #print(noise)
    state = np.array([-8.0, 0.0])
    state_sequence = np.array([state])
    print(state,state_sequence)
    for i in range(100):
        state, _, _, Flag_Collision = env.step(state, np.array([0,0]), noise[i])
        print(Flag_Collision)
        if Flag_Collision:
            print('Collision', state)
            break
        else:
            state_sequence = np.append(state_sequence, [state], axis=0)
    print(state_sequence)

    #ax0.plot(state_sequence[:,0], smooth(state_sequence[:,1], 10))
    ax0.plot(state_sequence[:,0], state_sequence[:,1])
    plt.rcParams.update({'font.size': font_size})
    plt.show()
