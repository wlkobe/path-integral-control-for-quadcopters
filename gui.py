import numpy as np
import math
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as Axes3D
import sys

# Plot Circle im 3D Environment
from matplotlib.patches import Circle, Ellipse
import mpl_toolkits.mplot3d.art3d as art3d
from matplotlib.font_manager import FontProperties
from parameters import Env_Parameters

class GUI():
    # 'quad_list' is a dictionary of format:
    # quad_list = {'quad_1_name':{'position':quad_1_position,'orientation':quad_1_orientation,'arm_span':quad_1_arm_span}, ...}
    def __init__(self, Parameters):

        self.Flag_GUI = Parameters['Flag_GUI']
        self.Flag_Task = Parameters['Task']
        #self.Flag_Task = 'Ring'
        #self.Flag_Task = 'Path'

        self.quads = Parameters['QUADCOPTER']
        self.fig0 = plt.figure(1, figsize = (8,6))
        self.ax = Axes3D.Axes3D(self.fig0)
        #self.fig1 = plt.figure(2, figsize = (8,16))
        limit = 3.0
        font_size = 12
        self.ax.set_xlim3d([-limit, limit])
        self.ax.set_xlabel('X [m]', size = font_size)
        self.ax.set_ylim3d([-limit, limit])
        self.ax.set_ylabel('Y [m]', size = font_size)
        self.ax.set_zlim3d([0, 5.0])
        self.ax.set_zlabel('Z [m]', size = font_size)
        #self.ax.set_title('Quadcopter Simulation', size = font_size*1.5)
        plt.xticks(fontsize=font_size)
        plt.yticks(fontsize=font_size)
        #self.ax.set_zticks(fontsize=font_size)
        #FontProperties(font_size)
        #self.ax.set_fontsize(font_size)
        if self.Flag_GUI:
            self.init_plot()
        """
        self.bx1 = self.fig1.add_subplot(411)
        plt.title('Position')
        self.bx2 = self.fig1.add_subplot(412)
        plt.title('Velocity')
        self.bx3 = self.fig1.add_subplot(413)
        plt.title('Angle')
        self.bx4 = self.fig1.add_subplot(414)
        plt.title('Control Output')
        """
        # Trajektry of the Quadcopter
        self.path = [[],[],[]]
        self.color_ring = 'k'

    def rotation_matrix(self,angles):
        ct = math.cos(angles[0])
        cp = math.cos(angles[1])
        cg = math.cos(angles[2])
        st = math.sin(angles[0])
        sp = math.sin(angles[1])
        sg = math.sin(angles[2])
        R_x = np.array([[1,0,0],[0,ct,-st],[0,st,ct]])
        R_y = np.array([[cp,0,sp],[0,1,0],[-sp,0,cp]])
        R_z = np.array([[cg,-sg,0],[sg,cg,0],[0,0,1]])
        R = np.dot(R_z, np.dot( R_y, R_x ))
        return R

    def init_plot(self):
        for key in self.quads:
            self.quads[key]['l1'], = self.ax.plot([],[],[],color='blue',linewidth=3,antialiased=False)
            self.quads[key]['l2'], = self.ax.plot([],[],[],color='red',linewidth=3,antialiased=False)
            self.quads[key]['hub'], = self.ax.plot([],[],[],marker='o',color='green', markersize=6,antialiased=False)
            self.quads[key]['path'] = self.ax.plot([],[],[],'-b',antialiased=False)

    def update_quad(self, key):
        R = self.rotation_matrix(self.quads[key]['orientation'])
        L = self.quads[key]['L']
        # Points of Arms in the body frame
        points = np.array([ [-L,0,0], [L,0,0], [0,-L,0], [0,L,0], [0,0,0], [0,0,0] ]).T
        # Transform the Points of Arms to the inertial frame
        points = np.dot(R,points)
        # Plus the position of quadcopter
        points[0,:] += self.quads[key]['position'][0]   # x-axis
        points[1,:] += self.quads[key]['position'][1]   # y-axis
        points[2,:] += self.quads[key]['position'][2]   # z-axis
        return points

    def new_line(self):
        self.path = [[],[],[]]

    def update(self):
        for key in self.quads:
            points = self.update_quad(key)
            # Plot the quadcopter
            self.quads[key]['l1'].set_data(points[0,0:2],points[1,0:2])
            self.quads[key]['l1'].set_3d_properties(points[2,0:2])
            self.quads[key]['l2'].set_data(points[0,2:4],points[1,2:4])
            self.quads[key]['l2'].set_3d_properties(points[2,2:4])
            self.quads[key]['hub'].set_data(points[0,5],points[1,5])
            self.quads[key]['hub'].set_3d_properties(points[2,5])

            # Plot the path
            self.path[0].append(self.quads[key]['position'][0])
            self.path[0] = self.path[0][-400:]
            self.path[1].append(self.quads[key]['position'][1])
            self.path[1] = self.path[1][-400:]
            self.path[2].append(self.quads[key]['position'][2])
            self.path[2] = self.path[2][-400:]
            line = self.ax.plot(self.path[0], self.path[1], self.path[2], '-b')

            # Plot the obstacles
            #p = Circle((0, 2), 1)

            # Plot the Ring Obstacle
            if self.Flag_Task == 'Ring':
                d_a = 2.5#1.5
                d_b = 1.3#0.8
                elp = Ellipse((0, 2), d_a, d_b, angle=45, linewidth=2, fill=False, zorder=2, color = self.color_ring)
                elp.set_alpha(0.4)
                self.ax.add_patch(elp)
                art3d.pathpatch_2d_to_3d(elp, z=0, zdir="y")
            elif self.Flag_Task == 'Path':
                # Plot the Ring Path
                d_a = 4#4.2
                d_b = 4#3.4
                elp = Ellipse((0, 0), d_a, d_b, angle=0, linewidth=2, fill=False, zorder=2, color = self.color_ring)
                elp.set_alpha(0.4)
                self.ax.add_patch(elp)
                art3d.pathpatch_2d_to_3d(elp, z=2, zdir="z")

        plt.pause(0.000000000000001)
        if self.Flag_Task == 'Ring' or self.Flag_Task == 'Path':
            elp.remove()
        line.pop(0).remove()

    def save_figure(self, env, task, time, epoch):
        plt.savefig('Logger_{}_{}_{}_{}.eps'.format(env, task, time, epoch), format='eps')
        plt.savefig('Logger_{}_{}_{}_{}.png'.format(env, task, time, epoch), format='png')

if __name__ == "__main__":

    labels = ['test']
    filename = ['54_Logger_Quadcopter_Goal_20200323100128']
    filepath = 'C:\\Users\\wlkob\\OneDrive\\8_Masterarbeit\\Programs Masterarbeit\\xExperiment\\Logger\\'

    Env = 'Quadcopter'
    seq_pos = np.linspace([-1.5, -1.5, 0.0], [1.5, 1.5, 4], 10)
    Env_Parameters[Env]['QUADCOPTER'] = {}
    for i in range(len(seq_pos)):
        Env_Parameters[Env]['QUADCOPTER'][i] = {'position':seq_pos[i], 'orientation':[0,0,0], 'L':0.3,'r':0.1,'prop_size':[10,4.5],'weight':1.2}
    #Env_Parameters[Env]['QUADCOPTER'] = {'q1':{'position':[-1.5, -1.5, 0.0],'orientation':[0,0,0],'L':0.3,'r':0.1,'prop_size':[10,4.5],'weight':1.2}}
    gui = GUI(Env_Parameters[Env])
    gui.Flag_GUI = True
    gui.init_plot()
    gui.update()
    plt.show()
