import numpy as np
import pandas as pd
import sys
#sys.path.append("..")
from logger import Logger
import matplotlib.pyplot as plt
import gui as GUI
from parameters import Env_Parameters


def check_collision_obstacle(gui, state):
    Flag_Collision_L1 = False
    Flag_Collision_L2 = False
    alpha = np.deg2rad(45)
    d_a = 1.5
    d_b = 0.8
    gui.quads['q1']['position'] = state[0:3]
    gui.quads['q1']['orientation'] = state[6:9]
    points = gui.update_quad('q1')
    # Check if y-axis of L1 cross the y=0
    if (points[1,0]*points[1,1] <= 0) & (points[1,0] != points[1,1]):
        # Calcualte the point of L1 on y=0
        x1 = -points[1,1]/(points[1,0]-points[1,1])*(points[0,0]-points[0,1])+points[0,1]
        z1 = -points[1,1]/(points[1,0]-points[1,1])*(points[2,0]-points[2,1])+points[2,1]

        # Check if collision the ring
        if (x1*np.cos(alpha)+(z1-2)*np.sin(alpha))**2/(d_a/2)**2 + (x1*np.sin(alpha)-(z1-2)*np.cos(alpha))**2/(d_b/2)**2 >= 1:
            Flag_Collision_L1 = True

    # Check if y-axis of L2 cross the y=0
    if (points[1,2]*points[1,3] <= 0) & (points[1,2] != points[1,3]):
        # Calcualte the point of L2 on y=0
        x2 = -points[1,3]/(points[1,2]-points[1,3])*(points[0,2]-points[0,3])+points[0,3]
        z2 = -points[1,3]/(points[1,2]-points[1,3])*(points[2,2]-points[2,3])+points[2,3]

        # Check if collision the ring
        if (x2*np.cos(alpha)+(z2-2)*np.sin(alpha))**2/(d_a/2)**2 + (x2*np.sin(alpha)-(z2-2)*np.cos(alpha))**2/(d_b/2)**2 >= 1:
            Flag_Collision_L2 = True

    # Check if Collision
    if (Flag_Collision_L1 is True) or (Flag_Collision_L2 is True):
        return True
    else:
        return False


def string2list_Quad(temp):
    temp_result = []
    for i in range(len(temp)):
        # Quadcopter
        temp_value = temp[i].replace('\r\n','').replace('[', '').replace(']', '').split(' ')
        temp_value = [float(x) for x in temp_value if x != '']
        #print(temp_value)
        temp_result.append(temp_value)
        #print(temp_result)
    return np.array(temp_result)

def cost_computing(task, state):
    if task == 1:
        Weight_Matrix_t = np.diag([50,50,80,10,10,10,5,5,5,1,1,1])
        GOAL = np.array([1.5,1.5,4.0, 0,0,0,0,0,0,0,0,0])
        cost = (0.5 * (state - GOAL).dot(Weight_Matrix_t).dot(state - GOAL))
    return cost

def result_process(log, len_seq, flag_combine):
    len_seq_all = []
    state_sequence_all = []#np.array([])
    cost_sequence_all = []#np.array([])
    for j in log.df.Epoch.unique():
        state_seq = log.df[(log.df['Phase'] == 'Update') & (log.df['Parameter'] == 'State_Init') & (log.df['Epoch'] == j)].reset_index(drop=True)['Value']
        if flag_combine:
            sequence = string2list_Quad(state_seq)[:len_seq]
        else:
            sequence = string2list_Quad(state_seq)#[:len_seq]
        cost_sequence = np.array([])
        for i in range(len(sequence)):
            state = sequence[i]
            cost = cost_computing(1, state)
            cost_sequence = np.append(cost_sequence, cost)
        len_seq_all.append(len(state_seq))
        cost_sequence_all.append(np.array(cost_sequence))
        state_sequence_all.append(np.array(sequence))
    print(len_seq_all)
    return np.array(state_sequence_all), np.array(cost_sequence_all)

if False:
    # Compare the Noise in Angle
    labels = ['0.1', '0.5', '1', '2', '5', '10']#, '0.5'
    filename = ['Logger_Quadcopter_Goal_20200319231041',
               'Logger_Quadcopter_Goal_20200320124151',
               'Logger_Quadcopter_Goal_20200320155533',
               'Logger_Quadcopter_Goal_20200320190407',
               'Logger_Quadcopter_Goal_20200320220200',
                'Logger_Quadcopter_Goal_20200321012306']
elif False:
    # Compare the Noise in Thrust
    labels = ['1', '5', '10', '40']
    filename = ['Logger_Quadcopter_Goal_20200321024452',
                'Logger_Quadcopter_Goal_20200321050755',
                'Logger_Quadcopter_Goal_20200321075416',
                'Logger_Quadcopter_Goal_20200321114354']
elif False:
    # Compare the Receding Horizon in Ring Environment
    labels = ['5', '10', '30', '50', '80', '100']
    labels = [50]
    filename = ['Logger_Quadcopter_Ring_20200320124224']
                #'Logger_Quadcopter_Ring_20200319231159',
                #'Logger_Quadcopter_Ring_20200319232510',
                #'Logger_Quadcopter_Ring_20200319234907',
                #'Logger_Quadcopter_Ring_20200320124224',
                #'Logger_Quadcopter_Ring_20200320183644',
                #'Logger_Quadcopter_Ring_20200321040342']
    filename = ['Logger_Quadcopter_Path_20200320191741']
elif True:
    # Ring Environment
    labels = ['test']
    filename = ['135_Logger_Quadcopter_Ring_20200330003058']



if __name__ == "__main__":

    len_seq = 149
    filepath = 'C:\\Users\\wlkob\\OneDrive\\8_Masterarbeit\\Programs Masterarbeit\\xExperiment\\Logger\\'
    #filepath = 'C:\\Users\\wlkob\\OneDrive\\8_Masterarbeit\\Programs Masterarbeit\\Logger\\'
    Env = 'Quadcopter'
    Env_Parameters[Env]['Task'] = 'Ring'

    log = [ Logger() for _ in range(len(filename))]
    for i in range(len(filename)):
        log[i].enable_logger()
        log[i].file_read(filepath + filename[i] + '.csv')

    if False:

        Env_Parameters[Env]['QUADCOPTER'] = {}
        for k in range(len(filename)):
            state_sequence_all, cost_sequence_all = result_process(log[k], len_seq, False)
            doc = np.array(pd.read_csv('list.csv').values.tolist())
            for j in range(len(cost_sequence_all)):
                print(j)
                #j = 0
                for i in range(0, len(state_sequence_all[j]), 5):
                    Env_Parameters[Env]['QUADCOPTER'][i] = {'position':state_sequence_all[j][i,:3], 'orientation':state_sequence_all[j][i,6:9], 'L':0.3,'r':0.1,'prop_size':[10,4.5],'weight':1.2}
                    #Env_Parameters[Env]['QUADCOPTER'][i] = {'position':doc[i,:3], 'orientation':doc[i,6:9], 'L':0.3,'r':0.1,'prop_size':[10,4.5],'weight':1.2}

        gui = GUI.GUI(Env_Parameters[Env])
        gui.Flag_GUI = True

        gui.init_plot()
        gui.update()
        plt.show()


    elif True:

        gui = GUI.GUI(Env_Parameters[Env])
        gui.Flag_GUI = True

        gui.init_plot()
        gui.update()

        for k in range(1):#len(filename)):
            k = 0
            state_sequence_all, cost_sequence_all = result_process(log[k], len_seq, False)
            print(labels[k], filename[k])
            for j in range(len(cost_sequence_all)):
                print(j)
                #j = 0
                for i in range(len(state_sequence_all[j])):
                    gui.quads['q1']['position'] = state_sequence_all[j][i,:3]
                    gui.quads['q1']['orientation'] = state_sequence_all[j][i,6:9]
                    gui.update()
                    if i == len(state_sequence_all[j])-1:
                        print('Final Position: {}'.format(state_sequence_all[j][i,:3]))
                        #gui.new_line()
                #print(check_collision_obstacle(gui, state_sequence_all[num_seq][i,:]))
        #plt.show()
