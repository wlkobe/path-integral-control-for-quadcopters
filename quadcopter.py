import numpy as np
import math
import scipy.integrate
import time
import datetime
import threading
import matplotlib.pyplot as plt

class Wind():
    def __init__(self, wind):

        self.dt = 0.02
        self.wind = wind
        # Mean wind speed -- V
        self.mean_speed = np.sqrt(wind['mean_speed'][0]**2 + wind['mean_speed'][1]**2 + wind['mean_speed'][2]**2)
        # Gradient height at where the maximal mean wind speed reaches -- z_g
        self.gradient_height = wind['gradient_height']
        # Height -- z
        self.height = 1

        # Parameter of Scale Length & Turbulence Intensities
        self.L = np.zeros(3)
        self.sigma = np.zeros(3)
        self.noise_cov = 1.0

        # Initial the ODE solver
        self.ode = scipy.integrate.ode(self.state_dot).set_integrator('vode', nsteps=500, method='bdf')
        self.time = datetime.datetime.now()

        # Initial the state of wind
        self.state = np.zeros(5)# + np.array([wind['mean_speed'][0], wind['mean_speed'][1], 0.0, wind['mean_speed'][2], 0.0])

    def update_para_scale_lengths(self, height):
        L_u = 280 * (height/self.gradient_height)**0.35
        L_v = 140 * (height/self.gradient_height)**0.48
        L_w = 0.35 * height
        self.L = np.array([L_u, L_v, L_w])
        return self.L

    def update_para_turbulence_intensities(self, height):
        sigma_u = (3.51 * (1/height)**0.01 - 3.23) * self.mean_speed
        sigma_v = (2.13 * (1/height)**0.01 - 1.95) * self.mean_speed
        sigma_w = (1.17 * (1/height)**0.01 - 1.06) * self.mean_speed
        self.sigma = np.array([sigma_u, sigma_v, sigma_w])
        return self.sigma

    def update_para(self, height):
        L = self.update_para_scale_lengths(height)
        sigma = self.update_para_turbulence_intensities(height)
        print('Parameters L: {}, Sigma: {}'.format(L,sigma))

    def state_dot(self, time, state, random):
        state_dot = np.zeros(5)
        # Velocity of u_g
        state_dot[0] = - self.mean_speed/self.L[0] * state[0] + self.sigma[0] * np.sqrt(3*self.mean_speed/self.L[0]) * random[0]
        # Velocity of v_g
        state_dot[1] = state[2] + self.sigma[1] * np.sqrt(3*self.mean_speed/self.L[1]) * random[1]
        state_dot[2] = - (self.mean_speed**2)/(self.L[1]**2) * state[1] - 2*(self.mean_speed/self.L[1]) * state[2] + ((1-2*np.sqrt(3))*self.sigma[1]*np.sqrt((self.mean_speed/self.L[1])**3)) * random[1]
        # Velocity of w_g
        state_dot[3] = state[4] + self.sigma[2] * np.sqrt(3*self.mean_speed/self.L[2]) * random[2]
        state_dot[4] = - (self.mean_speed**2)/(self.L[2]**2) * state[3] - 2*(self.mean_speed/self.L[2]) * state[4] + ((1-2*np.sqrt(3))*self.sigma[2]*np.sqrt((self.mean_speed/self.L[2])**3)) * random[2]
        return state_dot

    def update(self, dt):
        self.ode.set_initial_value(self.state, 0).set_f_params(np.random.normal([0,0,0],[self.noise_cov,self.noise_cov,self.noise_cov]))
        self.state = self.ode.integrate(self.ode.t + dt)
        return np.array([self.state[0]+self.wind['mean_speed'][0], self.state[1]+self.wind['mean_speed'][1], self.state[3]+self.wind['mean_speed'][2]])

class Propeller():
    def __init__(self, prop_dia, prop_pitch, thrust_unit='N'):
        self.dia = prop_dia
        self.pitch = prop_pitch
        self.thrust_unit = thrust_unit
        self.speed = 0 #RPM
        self.thrust = 0

    def set_speed(self,speed):
        self.speed = speed
        # From http://www.electricrcaircraftguy.com/2013/09/propeller-static-dynamic-thrust-equation.html
        self.thrust = 4.392e-8 * self.speed * math.pow(self.dia,3.5)/(math.sqrt(self.pitch))
        self.thrust = self.thrust*(4.23e-4 * self.speed * self.pitch)
        if self.thrust_unit == 'Kg':
            self.thrust = self.thrust*0.101972

class Quadcopter():
    # State space representation: [x y z x_dot y_dot z_dot theta phi gamma theta_dot phi_dot gamma_dot]
    # From Quadcopter Dynamics, Simulation, and Control by Andrew Gibiansky
    def __init__(self,Parameters,gravity=9.81,b=0.0245):

        self.FLAG_Dynamic = True

        self.wind = Wind(Parameters['Wind_Para'])
        self.wind.update_para(height = 2)
        self.wind.update(dt = 0.02)
        self.Flag_Wind = False
        self.Flag_NegativeThrust = Parameters['Flag_NegativeThrust']

        self.quads = Parameters['QUADCOPTER']
        self.g = gravity
        self.b = b
        self.thread_object = None

        self.C_d = 1.05
        self.rho = 1.225
        self.A_side = (np.sqrt(2.0) * self.quads['q1']['L']) * 0.1
        self.A_top = (np.sqrt(2.0) * self.quads['q1']['L'])**2

        if self.FLAG_Dynamic:
            self.ode =  scipy.integrate.ode(self.state_dot).set_integrator('vode',nsteps=500,method='bdf')
        else:
            self.ode =  scipy.integrate.ode(self.state_dot_particle).set_integrator('vode',nsteps=500,method='bdf')

        self.time = datetime.datetime.now()
        for key in self.quads:
            self.quads[key]['state'] = np.zeros(12)
            self.quads[key]['state'][0:3] = self.quads[key]['position']
            self.quads[key]['state'][6:9] = self.quads[key]['orientation']
            self.quads[key]['m1'] = Propeller(self.quads[key]['prop_size'][0],self.quads[key]['prop_size'][1])
            self.quads[key]['m2'] = Propeller(self.quads[key]['prop_size'][0],self.quads[key]['prop_size'][1])
            self.quads[key]['m3'] = Propeller(self.quads[key]['prop_size'][0],self.quads[key]['prop_size'][1])
            self.quads[key]['m4'] = Propeller(self.quads[key]['prop_size'][0],self.quads[key]['prop_size'][1])
            # From Quadrotor Dynamics and Control by Randal Beard
            ixx=((2*0.6*self.quads[key]['weight']*self.quads[key]['r']**2)/5)+(2*0.1*self.quads[key]['weight']*(self.quads[key]['L'])**2)
            iyy=ixx
            izz=((2*0.6*self.quads[key]['weight']*self.quads[key]['r']**2)/5)+(4*0.1*self.quads[key]['weight']*(self.quads[key]['L'])**2)
            self.quads[key]['I'] = np.array([[ixx,0,0],[0,iyy,0],[0,0,izz]])
            self.quads[key]['invI'] = np.linalg.inv(self.quads[key]['I'])
        self.run = True

    def matrix_normal_mppi(self, key):
        m = self.quads[key]['weight']
        invI_xx = self.quads[key]['invI'][0][0]
        invI_yy = self.quads[key]['invI'][1][1]
        invI_zz = self.quads[key]['invI'][2][2]
        G_c = np.array([[0,0,0,0], [0,0,0,0], [0,0,0,1/m], [invI_xx,0,0,0], [0, invI_yy, 0,0], [0,0,invI_zz,0]])
        B_c = np.array([[1/m,0,0],[0,1/m,0],[0,0,1/m],[0,0,0],[0,0,0],[0,0,0]])
        return G_c, B_c

    def rotation_matrix_R(self,angles):
        ct = math.cos(angles[0])
        cp = math.cos(angles[1])
        cg = math.cos(angles[2])
        st = math.sin(angles[0])
        sp = math.sin(angles[1])
        sg = math.sin(angles[2])
        R_x = np.array([[1,0,0],[0,ct,-st],[0,st,ct]])
        R_y = np.array([[cp,0,sp],[0,1,0],[-sp,0,cp]])
        R_z = np.array([[cg,-sg,0],[sg,cg,0],[0,0,1]])
        R = np.dot(R_z, np.dot( R_y, R_x ))
        return R

    def rotation_matrix_E(self,angles):
        ct = math.cos(angles[0])
        cp = math.cos(angles[1])
        st = math.sin(angles[0])
        sp = math.sin(angles[1])
        tp = math.tan(angles[1])
        E = np.array([[1, st*tp, ct*tp],[0, ct, -st],[0, st/cp, ct/cp]])
        return E

    def wrap_angle(self,val):
        return( ( val + np.pi) % (2 * np.pi ) - np.pi )

    def state_dot_vehicle(self, time, state, key):
        state_dot = np.zeros(12)
        # get states / inputs
        x       = self.quads[key]['state'][0]
        y       = self.quads[key]['state'][1]
        psi     = self.quads[key]['state'][8]
        v       = np.sqrt(self.quads[key]['state'][3]**2 + self.quads[key]['state'][4]**2)
        d_f     = self.quads[key]['m1'].thrust
        a       = self.quads[key]['m2'].thrust

        # extract parameters
        (L_a, L_b) = (0.2, 0.1)

        # compute slip angle
        bta         = arctan( L_a / (L_a + L_b) * tan(d_f) )

        # compute next state
        state_dot[0]      = ( v*cos(psi + bta) )
        state_dot[1]      = ( v*sin(psi + bta) )
        state_dot[3]      = a*cos(psi + bta)
        state_dot[4]      = a*sin(psi + bta)
        state_dot[8]      = v/L_b*sin(bta)
        return state_dot

    def state_dot_particle(self, time, state, key):
        state_dot = np.zeros(12)
        state_dot[0] = self.quads[key]['state'][3]
        state_dot[1] = self.quads[key]['state'][4]
        state_dot[2] = self.quads[key]['state'][5]
        # The velocities(t+1 x_dots equal the t x_dots)
        state_dot[3] = self.quads[key]['m1'].thrust
        state_dot[4] = self.quads[key]['m2'].thrust
        state_dot[5] = self.quads[key]['m3'].thrust
        return state_dot

    def state_dot_new_u(self, time, state, key, wind):
    #def state_dot(self, time, state, key, wind):
        state_dot = np.zeros(12)
        # The velocities in the earth frame (t+1 x_dots equal the t x_dots)
        x_dot = np.dot(self.rotation_matrix_R(self.quads[key]['state'][6:9]), self.quads[key]['state'][3:6])
        state_dot[0] = x_dot[0]
        state_dot[1] = x_dot[1]
        state_dot[2] = x_dot[2]

        # The acceleration in the body frame
        #relative_wind =  np.dot(self.rotation_matrix_R(self.quads[key]['state'][6:9]).T, wind) - self.quads[key]['state'][3:6]
        relative_wind = np.dot(self.rotation_matrix_R(self.quads[key]['state'][6:9]).T, wind) - self.quads[key]['state'][3:6]
        F_wind = 1/2 * self.C_d * self.rho * np.array([np.sign(relative_wind[0])*self.A_side*relative_wind[0]**2, np.sign(relative_wind[1])*self.A_side*relative_wind[1]**2, np.sign(relative_wind[2])*self.A_top*relative_wind[2]**2])
        print(F_wind)
        #x_dotdot = np.array([0,0,-self.quads[key]['weight']*self.g]) + np.dot(self.rotation_matrix(self.quads[key]['state'][6:9]),np.array([0,0,(self.quads[key]['m1'].thrust + self.quads[key]['m2'].thrust + self.quads[key]['m3'].thrust + self.quads[key]['m4'].thrust)]))/self.quads[key]['weight']
        x_dotdot =  - np.cross(self.quads[key]['state'][9:12], self.quads[key]['state'][3:6]) + \
                    np.dot(self.rotation_matrix_R(self.quads[key]['state'][6:9]).T, np.array([0,0,-self.g])) + \
                    1/self.quads[key]['weight'] * (np.array([0,0,(self.quads[key]['m1'].thrust + self.quads[key]['m2'].thrust + self.quads[key]['m3'].thrust + self.quads[key]['m4'].thrust)]) + F_wind)# + \
                                                    #1/2 * self.C_d * self.rho * np.array([self.A_side*relative_wind[0]**2, self.A_side*relative_wind[1]**2, self.A_top*relative_wind[2]**2]))
        print(x_dotdot)
        state_dot[3] = x_dotdot[0]
        state_dot[4] = x_dotdot[1]
        state_dot[5] = x_dotdot[2]

        # The angular rates in the earth frame(t+1 theta_dots equal the t theta_dots)
        theta_dot = np.dot(self.rotation_matrix_E(self.quads[key]['state'][6:9]), self.quads[key]['state'][9:12])
        state_dot[6] = theta_dot[0]#self.quads[key]['state'][9]
        state_dot[7] = theta_dot[1]#self.quads[key]['state'][10]
        state_dot[8] = theta_dot[2]#self.quads[key]['state'][11]

        # The angular accelerations in the body frame
        omega = self.quads[key]['state'][9:12]
        tau = np.array([self.quads[key]['L']*(self.quads[key]['m1'].thrust-self.quads[key]['m3'].thrust)
                        , self.quads[key]['L']*(self.quads[key]['m2'].thrust-self.quads[key]['m4'].thrust)
                        , self.b*(self.quads[key]['m1'].thrust-self.quads[key]['m2'].thrust+self.quads[key]['m3'].thrust-self.quads[key]['m4'].thrust)])
        omega_dot = np.dot(self.quads[key]['invI'], (tau - np.cross(np.float64(omega), np.float64(np.dot(self.quads[key]['I'],omega)))))
        state_dot[9] = omega_dot[0]
        state_dot[10] = omega_dot[1]
        state_dot[11] = omega_dot[2]
        return state_dot

    #def state_dot_new(self, time, state, key, wind):
    def state_dot(self, time, state, key, wind):
        state_dot = np.zeros(12)
        # The velocities in the earth frame (t+1 x_dots equal the t x_dots)
        x_dot = np.dot(self.rotation_matrix_R(self.quads[key]['state'][6:9]), self.quads[key]['state'][3:6])
        state_dot[0] = x_dot[0]
        state_dot[1] = x_dot[1]
        state_dot[2] = x_dot[2]

        # The acceleration in the body frame
        relative_wind = np.dot(self.rotation_matrix_R(self.quads[key]['state'][6:9]).T, wind) - self.quads[key]['state'][3:6]
        F_wind = 1/2 * self.C_d * self.rho * np.array([np.sign(relative_wind[0])*self.A_side*relative_wind[0]**2, np.sign(relative_wind[1])*self.A_side*relative_wind[1]**2, np.sign(relative_wind[2])*self.A_top*relative_wind[2]**2])
        #x_dotdot = np.array([0,0,-self.quads[key]['weight']*self.g]) + np.dot(self.rotation_matrix(self.quads[key]['state'][6:9]),np.array([0,0,(self.quads[key]['m1'].thrust + self.quads[key]['m2'].thrust + self.quads[key]['m3'].thrust + self.quads[key]['m4'].thrust)]))/self.quads[key]['weight']
        x_dotdot =  - np.cross(np.float64(self.quads[key]['state'][9:12]), np.float64(self.quads[key]['state'][3:6])) + \
                    np.dot(self.rotation_matrix_R(self.quads[key]['state'][6:9]).T, np.array([0,0,-self.g])) + \
                    1/self.quads[key]['weight'] * (np.array([0,0,self.quads[key]['m4'].thrust]) + F_wind)
        state_dot[3] = x_dotdot[0]
        state_dot[4] = x_dotdot[1]
        state_dot[5] = x_dotdot[2]

        # The angular rates in the earth frame(t+1 theta_dots equal the t theta_dots)
        theta_dot = np.dot(self.rotation_matrix_E(self.quads[key]['state'][6:9]), self.quads[key]['state'][9:12])
        state_dot[6] = theta_dot[0]#self.quads[key]['state'][9]
        state_dot[7] = theta_dot[1]#self.quads[key]['state'][10]
        state_dot[8] = theta_dot[2]#self.quads[key]['state'][11]

        # The angular accelerations in the body frame
        omega = self.quads[key]['state'][9:12]
        tau = np.array([self.quads[key]['m1'].thrust
                        , self.quads[key]['m2'].thrust
                        , self.quads[key]['m3'].thrust])
        omega_dot = np.dot(self.quads[key]['invI'], (tau - np.cross(np.float64(omega), np.float64(np.dot(self.quads[key]['I'],omega)))))
        state_dot[9] = omega_dot[0]
        state_dot[10] = omega_dot[1]
        state_dot[11] = omega_dot[2]
        return state_dot

    def state_dot_old_u(self, time, state, key, wind):
    #def state_dot(self, time, state, key, wind):
        state_dot = np.zeros(12)
        # The velocities(t+1 x_dots equal the t x_dots)
        state_dot[0] = self.quads[key]['state'][3]
        state_dot[1] = self.quads[key]['state'][4]
        state_dot[2] = self.quads[key]['state'][5]

        # The acceleration
        relative_wind = np.dot(self.rotation_matrix_R(self.quads[key]['state'][6:9]).T, wind - self.quads[key]['state'][3:6])
        F_wind = 1/2 * self.C_d * self.rho * np.array([np.sign(relative_wind[0])*self.A_side*relative_wind[0]**2, np.sign(relative_wind[1])*self.A_side*relative_wind[1]**2, np.sign(relative_wind[2])*self.A_top*relative_wind[2]**2])
        #x_dotdot = np.array([0,0,-self.quads[key]['weight']*self.g]) + np.dot(self.rotation_matrix(self.quads[key]['state'][6:9]),np.array([0,0,(self.quads[key]['m1'].thrust + self.quads[key]['m2'].thrust + self.quads[key]['m3'].thrust + self.quads[key]['m4'].thrust)]))/self.quads[key]['weight']
        x_dotdot = np.array([0,0,-self.g]) + np.dot(self.rotation_matrix_R(self.quads[key]['state'][6:9]),np.array([0,0,(self.quads[key]['m1'].thrust + self.quads[key]['m2'].thrust + self.quads[key]['m3'].thrust + self.quads[key]['m4'].thrust)])+F_wind)/self.quads[key]['weight']
        state_dot[3] = x_dotdot[0]
        state_dot[4] = x_dotdot[1]
        state_dot[5] = x_dotdot[2]

        # The angular rates(t+1 theta_dots equal the t theta_dots)
        theta_dot = np.dot(self.rotation_matrix_E(self.quads[key]['state'][6:9]), self.quads[key]['state'][9:12])
        state_dot[6] = theta_dot[0]#self.quads[key]['state'][9]
        state_dot[7] = theta_dot[1]#self.quads[key]['state'][10]
        state_dot[8] = theta_dot[2]#self.quads[key]['state'][11]

        # The angular accelerations
        omega = self.quads[key]['state'][9:12]
        tau = np.array([self.quads[key]['L']*(self.quads[key]['m1'].thrust-self.quads[key]['m3'].thrust)
                        , self.quads[key]['L']*(self.quads[key]['m2'].thrust-self.quads[key]['m4'].thrust)
                        , self.b*(self.quads[key]['m1'].thrust-self.quads[key]['m2'].thrust+self.quads[key]['m3'].thrust-self.quads[key]['m4'].thrust)])
        omega_dot = np.dot(self.quads[key]['invI'], (tau - np.cross(np.float64(omega), np.float64(np.dot(self.quads[key]['I'],omega)))))
        state_dot[9] = omega_dot[0]
        state_dot[10] = omega_dot[1]
        state_dot[11] = omega_dot[2]
        return state_dot

    def state_dot_old(self, time, state, key, wind):
    #def state_dot(self, time, state, key, wind):
        state_dot = np.zeros(12)
        # The velocities(t+1 x_dots equal the t x_dots)
        state_dot[0] = self.quads[key]['state'][3]
        state_dot[1] = self.quads[key]['state'][4]
        state_dot[2] = self.quads[key]['state'][5]

        # The acceleration
        relative_wind =  np.dot(self.rotation_matrix_R(self.quads[key]['state'][6:9]).T, wind - self.quads[key]['state'][3:6])
        F_wind = np.array([0,0,0])#1/2*self.C_d*self.rho*np.array([np.sign(relative_wind[0])*self.A_side*relative_wind[0]**2, np.sign(relative_wind[1])*self.A_side*relative_wind[1]**2, np.sign(relative_wind[2])*self.A_top*relative_wind[2]**2])
        #x_dotdot = np.array([0,0,-self.quads[key]['weight']*self.g]) + np.dot(self.rotation_matrix(self.quads[key]['state'][6:9]),np.array([0,0,(self.quads[key]['m1'].thrust + self.quads[key]['m2'].thrust + self.quads[key]['m3'].thrust + self.quads[key]['m4'].thrust)]))/self.quads[key]['weight']
        x_dotdot = np.array([0,0,-self.g]) + np.dot(self.rotation_matrix_R(self.quads[key]['state'][6:9]),(np.array([0,0,self.quads[key]['m4'].thrust]) + F_wind))/self.quads[key]['weight']
        state_dot[3] = x_dotdot[0]
        state_dot[4] = x_dotdot[1]
        state_dot[5] = x_dotdot[2]

        # The angular rates(t+1 theta_dots equal the t theta_dots)
        theta_dot = np.dot(self.rotation_matrix_E(self.quads[key]['state'][6:9]), self.quads[key]['state'][9:12])
        state_dot[6] = theta_dot[0]#self.quads[key]['state'][9]
        state_dot[7] = theta_dot[1]#self.quads[key]['state'][10]
        state_dot[8] = theta_dot[2]#self.quads[key]['state'][11]

        # The angular accelerations
        omega = self.quads[key]['state'][9:12]
        tau = np.array([self.quads[key]['m1'].thrust
                        , self.quads[key]['m2'].thrust
                        , self.quads[key]['m3'].thrust])
        omega_dot = np.dot(self.quads[key]['invI'], (tau - np.cross(np.float64(omega), np.float64(np.dot(self.quads[key]['I'],omega)))))
        state_dot[9] = omega_dot[0]
        state_dot[10] = omega_dot[1]
        state_dot[11] = omega_dot[2]
        return state_dot

    def quad_dynamic(self, state, u):
        key = 'q1'
        # u = [Roll Torque, Pitch Torque, Yaw Torque, Thrust Force]
        state_dot = np.zeros(12)
        # The velocities(t+1 x_dots equal the t x_dots)
        state_dot[0] = state[3]
        state_dot[1] = state[4]
        state_dot[2] = state[5]

        # The acceleration
        #x_dotdot = np.array([0,0,-self.quads[key]['weight']*self.g]) + np.dot(self.rotation_matrix(self.quads[key]['state'][6:9]),np.array([0,0,(self.quads[key]['m1'].thrust + self.quads[key]['m2'].thrust + self.quads[key]['m3'].thrust + self.quads[key]['m4'].thrust)]))/self.quads[key]['weight']
        x_dotdot = np.array([0,0,-self.g]) + np.dot(self.rotation_matrix_R(state[6:9]),(np.array([0,0,u[3]])))/self.quads[key]['weight']
        state_dot[3] = x_dotdot[0]
        state_dot[4] = x_dotdot[1]
        state_dot[5] = x_dotdot[2]

        # The angular rates(t+1 theta_dots equal the t theta_dots)
        theta_dot = np.dot(self.rotation_matrix_E(state[6:9]), state[9:12])
        state_dot[6] = theta_dot[0]#self.quads[key]['state'][9]
        state_dot[7] = theta_dot[1]#self.quads[key]['state'][10]
        state_dot[8] = theta_dot[2]#self.quads[key]['state'][11]

        # The angular accelerations
        omega = state[9:12]
        tau = np.array([u[0]
                        , u[1]
                        , u[2]])
        omega_dot = np.dot(self.quads[key]['invI'], (tau - np.cross(np.float64(omega), np.float64(np.dot(self.quads[key]['I'],omega)))))
        state_dot[9] = omega_dot[0]
        state_dot[10] = omega_dot[1]
        state_dot[11] = omega_dot[2]
        return state_dot

    def update(self, dt):
        if self.Flag_Wind:
            wind = self.wind.update(dt)#np.array([0,0,0])#
        else:
            wind = np.array([0,0,0])#
        for key in self.quads:
            self.ode.set_initial_value(self.quads[key]['state'],0).set_f_params(key, wind)
            self.quads[key]['state'] = self.ode.integrate(self.ode.t + dt)
            # Simulate the Wind
            #self.quads['q1']['state'][3:6] += np.random.normal([-0.005,0,0], [0.1,0.1,0.1])
            self.quads[key]['state'][6:9] = self.wrap_angle(self.quads[key]['state'][6:9])
            self.quads[key]['state'][2] = max(0, self.quads[key]['state'][2])
            if (self.quads[key]['state'][2] == 0) and (self.quads[key]['state'][5] < 0):
                self.quads[key]['state'][5] = 0

    def set_motor_speeds(self,quad_name,speeds):
        self.quads[quad_name]['m1'].set_speed(speeds[0])
        self.quads[quad_name]['m2'].set_speed(speeds[1])
        self.quads[quad_name]['m3'].set_speed(speeds[2])
        self.quads[quad_name]['m4'].set_speed(speeds[3])

    def set_thrusts(self,quad_name,thrusts):
        self.quads[quad_name]['m1'].thrust = thrusts[0]
        self.quads[quad_name]['m2'].thrust = thrusts[1]
        self.quads[quad_name]['m3'].thrust = thrusts[2]
        if self.Flag_NegativeThrust:
            self.quads[quad_name]['m4'].thrust = thrusts[3]
        else:
            if thrusts[3] >= 0.0:
                self.quads[quad_name]['m4'].thrust = thrusts[3]
            else:
                self.quads[quad_name]['m4'].thrust = 0.0

    def get_position(self,quad_name):
        return self.quads[quad_name]['state'][0:3]

    def get_linear_rate(self,quad_name):
        return self.quads[quad_name]['state'][3:6]

    def get_orientation(self,quad_name):
        return self.quads[quad_name]['state'][6:9]

    def get_angular_rate(self,quad_name):
        return self.quads[quad_name]['state'][9:12]

    def get_state(self,quad_name):
        return self.quads[quad_name]['state']

    def set_position(self,quad_name,position):
        self.quads[quad_name]['state'][0:3] = position

    def set_orientation(self,quad_name,orientation):
        self.quads[quad_name]['state'][6:9] = orientation

    def get_time(self):
        return self.time

    def thread_run(self, dt, time_scaling):
        rate = time_scaling*dt
        last_update = self.time
        while(self.run==True):
            time.sleep(0)
            self.time = datetime.datetime.now()
            if (self.time-last_update).total_seconds() > rate:
                self.update(dt)
                last_update = self.time

    def start_thread(self,dt=0.002,time_scaling=1):
        self.thread_object = threading.Thread(target=self.thread_run, args=(dt,time_scaling))
        self.thread_object.start()

    def stop_thread(self):
        self.run = False

if __name__ == '__main__':
    Env_Parameters = {
        'Quadcopter':{
                        'Flag_GUI':False,
                        'Flag_Wind':True,
                        'Flag_ModelWind':True,
                        'Flag_NegativeThrust': False,
                        'Task':'Goal',
                        #'Task':'Ring',
                        #'Task':'Path',
                        'N':30,
                        'K':100,
                        'M':150,
                        'Noise_Cov':np.array([0.5,0.5,0.5,10]),
                        'State_Dim':12,
                        'Control_Dim':4,
                        'Control_Cost':0.001,
                        'Algorithm':'MPPI',
                        #'Algorithm':'Info_MPPI',
                        'Wind_Para':{'mean_speed': [-5, 0, 0], 'gradient_height': 100},
                        'QUADCOPTER':{'q1':{'position':[0,0, 0.0],'orientation':[0,0,0],'L':0.3,'r':0.1,'prop_size':[10,4.5],'weight':1.2}},
                        'GOAL':{'q1':{'position':[1.5,1.5,4.0], 'orientation':[0,0,0]}}
                }}

    if False:
        time = 60
        dt = 0.02
        # Test Wind Simulation
        wind_para = {'mean_speed': [-5,0,0], 'gradient_height': 100}
        wind = Wind(wind_para)
        wind.update_para(height = 2)
        wind.update(dt = dt)
        wind_state = np.array([wind.state])
        print(wind_state)
        for i in range(int(time/dt)):
            wind.update(dt=dt)
            wind_state = np.append(wind_state, np.array([wind.state]), axis = 0)
        print(wind_state)
        t = np.linspace(0, time, time/dt+1, endpoint=True)
        print(len(wind_state[:,0]))

        font_size = 16
        plt.figure(figsize = (12,8))
        plt.rcParams.update({'font.size': font_size})

        plt.subplot(311)
        plt.title("Simulation of Wind Speed", size = font_size)
        plt.plot(t, wind_state[:,0] + wind_para['mean_speed'][0], label = '${u_g}$')
        plt.legend(loc = 1)
        plt.ylim(wind_para['mean_speed'][0]-1, wind_para['mean_speed'][0]+1)

        plt.subplot(312)
        plt.plot(t, wind_state[:,1] + wind_para['mean_speed'][1], label = '${v_g}$')
        plt.ylabel('Wind Speed [m/s]')
        plt.legend(loc = 1)
        plt.ylim(wind_para['mean_speed'][1]-1, wind_para['mean_speed'][1]+1)

        plt.subplot(313)
        plt.plot(t, wind_state[:,3] + wind_para['mean_speed'][2], label = '${w_g}$')
        plt.legend(loc = 1)
        plt.ylim(wind_para['mean_speed'][2]-1, wind_para['mean_speed'][2]+1)

        plt.xlabel('Time [s]')
        plt.show()

    if True:
        Flag_Wind = False
        time = 10
        dt = 0.02
        t = np.linspace(0, time, time/dt+1, endpoint=True)
        import gui as GUI
        # Define the Quadcopter
        QUADCOPTER={'q1':{'position':[0,0,0],'orientation':[0,0,0],'L':0.3,'r':0.1,'prop_size':[8,4.5],'weight':1.2}}
        quad = Quadcopter(Env_Parameters['Quadcopter'])
        gui = GUI.GUI(Env_Parameters['Quadcopter'])
        states = np.array([quad.get_state('q1')])
        u = np.array([0.001, 0, 0, 15]) * np.ones((int(time/dt+1),4))
        #u[:,0] = 0.01*np.sin(t)
        for i in range(int(time/dt)):
            quad.set_thrusts('q1', u[i])
            quad.update(dt = 0.02)
            states = np.append(states, np.array([quad.get_state('q1')]), axis = 0)
        states_nowind = states


        Flag_Wind = True
        time = 10
        dt = 0.02
        t = np.linspace(0, time, time/dt+1, endpoint=True)
        # Define the Quadcopter
        #QUADCOPTER={'q1':{'position':[0,0,0],'orientation':[0,0,0],'L':0.3,'r':0.1,'prop_size':[8,4.5],'weight':1.2}}
        quad = Quadcopter(Env_Parameters['Quadcopter'])
        gui = GUI.GUI(Env_Parameters['Quadcopter'])
        states = np.array([quad.get_state('q1')])
        quad.Flag_Wind = True
        #u[:,0] = 0.01*np.sin(t)
        for i in range(int(time/dt)):
            quad.set_thrusts('q1', u[i])
            quad.update(dt = 0.02)
            states = np.append(states, np.array([quad.get_state('q1')]), axis = 0)
        states_wind = states

        font_size = 16
        fig_size = (14,4)
        """
        plt.figure(2, figsize = fig_size)
        #plt.subplot(511)
        plt.rcParams.update({'font.size': font_size})
        plt.plot(t, u[:,0], label = '$\\tau_{\phi}$')
        plt.plot(t, u[:,1], label = '$\\tau_{\\theta}$')
        plt.plot(t, u[:,2], label = '$\\tau_{\psi}$')
        plt.plot(t, u[:,3], label = '$F_{thrust}$')
        plt.legend( loc='upper right', bbox_to_anchor=(1.13, 1), borderaxespad =0.)
        #plt.legend(loc='upper right')
        plt.title('Simulation of Quadcopter in Wind Field')
        plt.ylabel('Control Output')
        #plt.xlabel('Time [s]', fontsize=font_size)
        plt.tight_layout()
        #plt.ylabel('Control Output')

        plt.figure(3, figsize = fig_size)
        #plt.subplot(512)
        plt.rcParams.update({'font.size': font_size})
        plt.plot(t, states_nowind[:,0], 'blue', linestyle = 'solid', label = '$x$ without wind')
        plt.plot(t, states_nowind[:,1], 'orange', linestyle = 'solid', label = '$y$ without wind')
        plt.plot(t, states_nowind[:,2], 'green', linestyle = 'solid', label = '$z$ without wind')
        plt.plot(t, states_wind[:,0], 'blue', linestyle = 'dotted', label = '$x$ with wind')
        plt.plot(t, states_wind[:,1], 'orange', linestyle = 'dotted', label = '$y$ with wind')
        plt.plot(t, states_wind[:,2], 'green', linestyle = 'dotted', label = '$z$ with wind')
        plt.legend( loc='upper right', bbox_to_anchor=(1.25, 1), borderaxespad =0.)
        plt.ylabel('Position')
        plt.tight_layout()
        #plt.legend(loc='upper left')
        #plt.xlabel('Time [s]', fontsize=font_size)
        #plt.ylabel('Positon')

        plt.figure(4, figsize = fig_size)
        #plt.subplot(513)
        plt.rcParams.update({'font.size': font_size})
        plt.plot(t, states_nowind[:,3], 'blue', linestyle = 'solid', label = '$u$ without wind')
        plt.plot(t, states_nowind[:,4], 'orange', linestyle = 'solid', label = '$v$ without wind')
        plt.plot(t, states_nowind[:,5], 'green', linestyle = 'solid', label = '$w$ without wind')
        plt.plot(t, states_wind[:,3], 'blue', linestyle = 'dotted', label = '$u$ with wind')
        plt.plot(t, states_wind[:,4], 'orange', linestyle = 'dotted', label = '$v$ with wind')
        plt.plot(t, states_wind[:,5], 'green', linestyle = 'dotted', label = '$w$ with wind')
        plt.legend( loc='upper right', bbox_to_anchor=(1.255, 1), borderaxespad =0.)
        plt.ylabel('Velocity')
        plt.tight_layout()
        #plt.legend(loc='upper left')
        #plt.ylabel('Control and States in Simulation', fontsize=14)

        plt.figure(5, figsize = fig_size)
        #plt.subplot(514)
        plt.rcParams.update({'font.size': font_size})
        plt.plot(t, states_nowind[:,6], 'blue', linestyle = 'solid', label = '$\phi$ without wind')
        plt.plot(t, states_nowind[:,7], 'orange', linestyle = 'solid', label = '$\\theta$ without wind')
        plt.plot(t, states_nowind[:,8], 'green', linestyle = 'solid', label = '$\psi$ without wind')
        plt.plot(t, states_wind[:,6], 'blue', linestyle = 'dotted', label = '$\phi$ with wind')
        plt.plot(t, states_wind[:,7], 'orange', linestyle = 'dotted', label = '$\\theta$ with wind')
        plt.plot(t, states_wind[:,8], 'green', linestyle = 'dotted', label = '$\psi$ with wind')
        plt.legend( loc='upper right', bbox_to_anchor=(1.255, 1), borderaxespad =0.)
        plt.ylabel('Angular')
        plt.tight_layout()
        #plt.legend(loc='upper left')
        """
        plt.figure(6, figsize = fig_size)
        #plt.subplot(515)
        plt.rcParams.update({'font.size': font_size})
        plt.plot(t, states_nowind[:,9], 'blue', linestyle = 'solid', label = '$\omega_{\phi}$ without wind')
        plt.plot(t, states_nowind[:,10], 'orange', linestyle = 'solid', label = '$\omega_{\\theta}$ without wind')
        plt.plot(t, states_nowind[:,11], 'green', linestyle = 'solid', label = '$\omega_{\psi}$ without wind')
        plt.plot(t, states_wind[:,9], 'blue', linestyle = 'dotted', label = '$\omega_{\phi}$ with wind')
        plt.plot(t, states_wind[:,10], 'orange', linestyle = 'dotted', label = '$\omega_{\\theta}$ with wind')
        plt.plot(t, states_wind[:,11], 'green', linestyle = 'dotted', label = '$\omega_{\psi}$ with wind')
        plt.legend( loc='upper right', bbox_to_anchor=(1.28, 1), borderaxespad =0.)
        plt.ylabel('Angular Velocity')
        plt.tight_layout()
        #plt.legend(loc='upper left')
        plt.xlabel('Time [s]', fontsize=font_size)


        plt.show()
        plt.pause(0.001)

        plt.figure(1)
        while(1):
            for i in range(int(time/dt)):
                gui.quads['q1']['position'] = states[i, 0:3]
                gui.quads['q1']['orientation'] = states[i, 6:9]
                gui.update()
