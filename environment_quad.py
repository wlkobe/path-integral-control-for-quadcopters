import matplotlib.pyplot as plt
import numpy as np
import random
import quadcopter as QUAD
import gui as GUI
from parameters import Env_Parameters

# TODO: Change GUI into Environment
class Environment():
    def __init__(self, Parameters):

        self.Flag_Task = Parameters['Task']

        self.gui = GUI.GUI(Parameters)
        self.quad = QUAD.Quadcopter(Parameters)
        # State space representation: [x y z x_dot y_dot z_dot theta phi gamma theta_dot phi_dot gamma_dot]
        self.INF = np.inf
        self.STATE_DIM = Parameters['State_Dim']
        self.CONTROL_DIM = Parameters['Control_Dim']
        self.STATE_INIT = np.zeros(self.STATE_DIM)
        self.STATE_INIT[0:3] = self.STATE_INIT[0:3] + Parameters['QUADCOPTER']['q1']['position']
        self.STATE_INIT[6:9] = self.STATE_INIT[6:9] + Parameters['QUADCOPTER']['q1']['orientation']
        self.GOAL = np.zeros(self.STATE_DIM)
        self.GOAL[0:3] = self.GOAL[0:3] + Parameters['GOAL']['q1']['position']
        self.GOAL[6:9] = self.GOAL[6:9] + Parameters['GOAL']['q1']['orientation']

        self.time = 0   # Current Time
        self.dt = 0.02
        self.state = self.STATE_INIT  # Current State
        self.counter = 0
        self.M = Parameters['M']   # Number of timesteps of process

        #self.time_sequence = np.linspace(0, 2, self.M)
        self.state_sequence = np.array([np.full(self.STATE_DIM, None) for _ in range(self.M)])
        self.control_sequence = np.array([np.full(self.CONTROL_DIM, None) for _ in range(self.M)])
        self.cost_sequence = np.ones(self.M)*np.inf
        self.state_sequence[0] = self.state
        self.control_sequence[0] = np.zeros(self.CONTROL_DIM)
        _, self.cost_sequence[0], _ = self.cost_path_calculate(self.state, np.zeros(self.CONTROL_DIM))

    def get_dim(self):
        return self.STATE_DIM, self.CONTROL_DIM

    def ellipse_ring(self, point, pos, size, angle):

        X = point[0]
        Y = point[1]

        if size[0] == 0:
            h = pos[1]
            k = pos[2]
            d_a = size[1]
            d_b = size[2]
        elif size[1] == 0:
            h = pos[0]
            k = pos[2]
            d_a = size[0]
            d_b = size[2]
        elif size[2] == 0:
            h = pos[0]
            k = pos[1]
            d_a = size[0]
            d_b = size[1]

        angle = np.deg2rad(angle)

        ellipse = ((((X)*np.cos(angle) + (Y)*np.sin(angle) - h)/(d_a/2))**2 + (((-(X)*np.sin(angle) + (Y)*np.cos(angle) - k)/(d_b/2))**2))

        return ellipse

    def check_collision_obstacle(self, state):
        Flag_Collision_L1 = False
        Flag_Collision_L2 = False
        Flag_Collision_Body = False

        pos_ring = [0, 0, 2]
        size_ring = [2.5, 0, 1.3]
        angle = 45

        d_a = size_ring[0]#2.5#1.5
        d_b = size_ring[2]#1.3#0.8
        alpha = np.deg2rad(angle)

        self.gui.quads['q1']['position'] = state[0:3]
        self.gui.quads['q1']['orientation'] = state[6:9]
        points = self.gui.update_quad('q1')

        # Check if collision the ring
        if (state[1] > -0.1 and state[1] < 0.1) and ((state[0]*np.cos(alpha)+(state[2]-2)*np.sin(alpha))**2/(d_a/2)**2 + (state[0]*np.sin(alpha)-(state[2]-2)*np.cos(alpha))**2/(d_b/2)**2 >= 1):
        #if  (state[1] > -0.1 and state[1] < 0.1) and self.ellipse_ring([state[0], state[2]], pos_ring, size_ring, angle) >= 1:
            Flag_Collision_Body = True

        # Check if y-axis of L1 cross the y=0
        if (points[1,0]*points[1,1] <= 0):# & (points[1,0] != points[1,1]):
            # Calcualte the point of L1 on y=0
            x1 = -points[1,1]/(points[1,0]-points[1,1])*(points[0,0]-points[0,1])+points[0,1]
            z1 = -points[1,1]/(points[1,0]-points[1,1])*(points[2,0]-points[2,1])+points[2,1]

            # Check if collision the ring
            if (x1*np.cos(alpha)+(z1-2)*np.sin(alpha))**2/(d_a/2)**2 + (x1*np.sin(alpha)-(z1-2)*np.cos(alpha))**2/(d_b/2)**2 >= 1:
            #if self.ellipse_ring([x1,z1], pos_ring, size_ring, angle) >= 1:
                Flag_Collision_L1 = True

            # Check if collision the wall
            #if x1<-1.7 or -0.7<x1<0.9 or x1>1.9:
            #    Flag_Collision_L1 = True

        # Check if y-axis of L2 cross the y=0
        if (points[1,2]*points[1,3] <= 0):# & (points[1,2] != points[1,3]):
            # Calcualte the point of L2 on y=0
            x2 = -points[1,3]/(points[1,2]-points[1,3])*(points[0,2]-points[0,3])+points[0,3]
            z2 = -points[1,3]/(points[1,2]-points[1,3])*(points[2,2]-points[2,3])+points[2,3]

            # Check if collision the ring
            if (x2*np.cos(alpha)+(z2-2)*np.sin(alpha))**2/(d_a/2)**2 + (x2*np.sin(alpha)-(z2-2)*np.cos(alpha))**2/(d_b/2)**2 >= 1:
            #if self.ellipse_ring([x2,z2], pos_ring, size_ring, angle) >= 1:
                Flag_Collision_L2 = True

            # Check if collision the wall
            #if x2<-1.7 or -0.7<x2<0.9 or x2>1.9:
            #    Flag_Collision_L1 = True

        # Check if collision the Wall
        #if -0.1<state[1]<0.1 and (state[0]<-1.7 or -0.7<state[0]<0.9 or state[0]>1.9):
        #    return True

        # Check if Collision
        if (Flag_Collision_L1 is True) or (Flag_Collision_L2 is True) or (Flag_Collision_Body is True):
            return True
        else:
            return False

    # Check if the particle collision
    def check_collision(self, state):
        Flag_Collision = False
        # Check if the particle aut of range
        r_wall = 5.0
        if state[0] < -r_wall or state[0] > r_wall or state[1] < -r_wall or state[1] > r_wall or state[2] < 0.0 or state[2] > 5.0:
            Flag_Collision = True
        # Check if collision the ring. If so, set the color to red, and keep the color
        if self.Flag_Task == 'Ring':
            if self.check_collision_obstacle(state):
                Flag_Collision = True

        # Check if the particle collision the wall
        #if (state[0] >= -1.0 and state[0] <= 1.0) and ((state[1] < -6.0) or (-4.0 < state[1] < 6.0) or (state[1] > 8.0)):
        #    Flag_Collision = True
        return Flag_Collision

    # Calculate the path cost function
    def cost_path_calculate(self, state, control_output):
        # Initial the cost of one trajectory
        cost_path = 0
        # Initial the flag of collision
        Flag_Collision = self.check_collision(state)
        # Check if the particle collision the wall or out of range
        if Flag_Collision:
            cost_path = self.INF

        v = np.sqrt(state[3]**2 + state[4]**2 + state[5]**2)
        distance = np.sqrt((state[:3] - self.GOAL[:3]).dot(state[:3] - self.GOAL[:3]))
        #state[6:] = np.rad2deg(state[6:])
        # cost of achieve the goal
        Weight_Matrix_t = np.diag([50,50,50,10,10,10,5,5,5,10,10,10])    # For Takeoff
        #Weight_Matrix_t = np.diag([50,50,80,10,10,10,5,5,5,1,1,1])    # For Takeoff
        Weight_Matrix_j = np.diag([2.5,2.5,2.5,0,0,50,1,1,1,0,0,0])           # For Journey
        Weight_Matrix_l = np.diag([10,10,10,100,100,25,5,5,150,0,0,0])         # For Landing

        if self.Flag_Task == 'Goal' or self.Flag_Task == 'Ring':
            # Soft Cost of Ring
            if False and self.Flag_Task == 'Ring':
                if (not Flag_Collision) and (state[1] > -0.3 and state[1] < 0.3):
                    pos_ring = [-0.8,0,2]
                    size_ring = [2.5, 0, 1.3]
                    angle = 45
                    cost_path -= (10 - self.ellipse_ring([state[0], state[2]], pos_ring, size_ring, angle) * 10)
            cost_path = cost_path + 0.5 * (state - self.GOAL).dot(Weight_Matrix_t).dot(state - self.GOAL)

        # cost of ellipse trajectory
        if self.Flag_Task == 'Path':
            v = np.sqrt(state[3]**2 + state[4]**2)# + state[5]**2)
            goal_v = 8
            d_a = 8#4.2
            d_b = 8#3.4
            #state[6:9] = np.rad2deg(state[6:9])
            #cost_path = cost_path + 100*(-np.sin(np.sqrt((state[0]/2.25)**2 + (state[1]/2.25)**2))) + 100*(state[2]-2)**2 + 20*(v - 2)**2 \
            #            + 0.5*(state[3:]).dot(state[3:])
            cost_path = cost_path + 100*np.abs((state[0]/(d_a/2))**2+(state[1]/(d_b/2))**2-1) + 100*(state[2]-2)**2 + 20*(v - goal_v)**2 + 20*state[5]**2 \
                        + 0.5*(state[3:]).dot(state[3:])
            #           0.5 * (state[8] - self.GOAL[8])**2 +  0.5 * state[3:5].dot(state[3:5]) + 0.5 * state[9:].dot(state[9:])

        cost_end = 0
        if self.Flag_Task == 'Goal' or self.Flag_Task == 'Ring':
            if distance < 0.5:
                cost_end = -1000

        return cost_end, cost_path, Flag_Collision

    # Define the STEP function
    def step0(self, state, control, noise):
        state = state + self.quad.quad_dynamic(state, (control+noise)) * self.dt
        if state[2] < 0.0:
            state[2] = 0.0
        cost_end, cost_path, Flag_Collision = self.cost_path_calculate(state, control+noise)
        return state, cost_end, cost_path, Flag_Collision

    def step(self, state, control, noise):
        # TODO: state should be:
        self.quad.quads['q1']['state'] = state
        # Set the thrust of motors
        self.quad.set_thrusts('q1', (control+noise))
        # Update the dynamic on the next timestep
        self.quad.update(self.dt)

        state = self.quad.quads['q1']['state']
        # Calculate the cost of this state
        cost_end, cost_path, Flag_Collision = self.cost_path_calculate(state, control+noise)
        return state, cost_end, cost_path, Flag_Collision

    def check_success(self, state):
        distance = np.sqrt((state[:3] - self.GOAL[:3]).dot(state[:3] - self.GOAL[:3]))
        if distance < 0.5:
            Flag_Success = True
        else:
            Flag_Success = False
        return Flag_Success

    # Update the state in one step
    def model_update(self, u):
        temp_state_sequence = np.array([np.full(self.STATE_DIM, None) for _ in range(self.M)])
        temp_control_sequence = np.array([np.zeros(self.CONTROL_DIM) for _ in range(self.M)])
        temp_cost_sequence = np.ones(self.M)*np.inf
        Flag_Success = False
        # Check if time over range
        if (self.time >= 0) and (self.counter < self.M-1):
            # If time not out of range, update the parameters
            self.time += 1*self.dt
            self.counter += 1
            Flag_Loop = False
        else:
            # If time over range, update a new loop
            # TODO: Control sequence is always new policy in the next epoch.
            Flag_Success = self.check_success(self.state)
            self.state = self.STATE_INIT
            self.quad.quads['q1']['state'] = self.state
            self.time = (0 + 1)*self.dt
            self.counter = 1
            temp_state_sequence = self.state_sequence
            temp_control_sequence = self.control_sequence
            temp_cost_sequence = self.cost_sequence
            self.cost_sequence = np.ones(self.M)*np.inf
            self.control_sequence = np.array([np.full(self.CONTROL_DIM, None) for _ in range(self.M)])
            self.state_sequence = np.array([np.full(self.STATE_DIM, None) for _ in range(self.M)])
            self.state_sequence[0] = self.state
            self.control_sequence[0] = np.zeros(self.CONTROL_DIM)
            _, self.cost_sequence[0], _ = self.cost_path_calculate(self.state, np.zeros(self.CONTROL_DIM))
            Flag_Loop = True
        # If collision, update a new loop
        if self.check_collision(self.state):
            Flag_Success = self.check_success(self.state)
            self.state = self.STATE_INIT
            self.quad.quads['q1']['state'] = self.state
            self.time = (0 + 1)*self.dt
            self.counter = 1
            temp_state_sequence = self.state_sequence
            temp_control_sequence = self.control_sequence
            temp_cost_sequence = self.cost_sequence
            self.cost_sequence = np.ones(self.M)*np.inf
            self.control_sequence = np.array([np.full(self.CONTROL_DIM, None) for _ in range(self.M)])
            self.state_sequence = np.array([np.full(self.STATE_DIM, None) for _ in range(self.M)])
            self.state_sequence[0] = self.state
            self.control_sequence[0] = np.zeros(self.CONTROL_DIM)
            _, self.cost_sequence[0], _ = self.cost_path_calculate(self.state, np.zeros(self.CONTROL_DIM))
            Flag_Loop = True
        else:
            self.state, _, _, _ = self.step(self.state, u, np.zeros(self.CONTROL_DIM))
            self.state_sequence[self.counter] = self.state
            self.control_sequence[self.counter] = u
            _, self.cost_sequence[self.counter], _ = self.cost_path_calculate(self.state, u)
            #self.state_sequence[int(self.time/self.dt)] = self.state
        print('Debug Model Update: Flag_Loop - {}, time - {}, counter - {}, M - {}'.format(Flag_Loop, self.time, self.counter, self.M))
        return Flag_Loop, temp_state_sequence, temp_control_sequence, temp_cost_sequence, Flag_Success

    # Plot one trajectory
    def plot_trajectory(self, state_sequence, color='-'):
        # TODO: Plot the trajectory in 3D room.
        self.gui.ax.plot(state_sequence[:,0], state_sequence[:,1], state_sequence[:,2], color=color, alpha = 0.5)

    # Plot the environment
    def render(self, key, position, orientation):
        self.gui.quads[key]['position'] = np.around(position, 3)
        self.gui.quads[key]['orientation'] = np.around(orientation, 3)
        self.gui.update()

if __name__ == "__main__":
    #QUADCOPTER={'q1':{'position':[1,0,2],'orientation':[0,0,0],'L':0.6,'r':0.1,'prop_size':[10,4.5],'weight':1.2}}
    Env = 'Quadcopter'
    Env_Parameters[Env]['Task'] = 'Goal'
    env = Environment(Env_Parameters[Env])
    font_size = 20
    state = np.zeros(12)
    state[1] = -0.11
    env.gui.Flag_GUI = True
    env.gui.init_plot()
    env.render('q1', [1,0,2], [0,0,0])
    """
    plt.pause(1)
    env.render('q1', [1,0,2], np.deg2rad([30,0,0]))
    plt.pause(1)
    env.render('q1', [1,0,2], np.deg2rad([0,0,0]))
    plt.pause(1)
    env.render('q1', [1,0,2], np.deg2rad([0,30,0]))
    plt.pause(1)
    env.render('q1', [1,0,2], np.deg2rad([0,0,0]))
    plt.pause(1)
    env.render('q1', [1,0,2], np.deg2rad([0,0,30]))
    """
    plt.rcParams.update({'font.size': font_size})
    plt.show()
    #print(env.check_collision_ring(state))
