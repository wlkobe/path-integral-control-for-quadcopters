import numpy as np

# For Normal MPPI
# 1D_Slit: 0.1, 2D_Slit: , Quad:
# For Info MPPI
# 1D_Slit: 0.0001, 2D_Slit: , Quad:

Env_Parameters = {
    'Quadcopter':{
                        'Flag_GUI':False,
                        'Task':'Goal',
                        #'Task':'Ring',
                        #'Task':'Path',
                        'N':30,
                        'K':100,
                        'M':150,
                        #'Noise_Cov':np.array([0.5,0.5,0.5,10]),
                        'Noise_Cov':np.array([1,1,1,40]),
                        'State_Dim':12,
                        'Control_Dim':4,
                        'Control_Cost':0.001,
                        'Algorithm':'MPPI',
                        #'Algorithm':'Info_MPPI',
                        'Wind_Para':{'mean_speed': [-10, 0, 0], 'gradient_height': 100},
                        'QUADCOPTER':{'q1':{'position':[-1.5, -1.5, 0.0],'orientation':[0,0,0],'L':0.3,'r':0.1,'prop_size':[10,4.5],'weight':1.2}},
                        'GOAL':{'q1':{'position':[1.5,1.5,4.0], 'orientation':[0,0,0]}},
                        'Flag_NegativeThrust': True
                        },
    'DoubleSlit1D':{
                        'Task':'CrossSlit',
                        'N':50,
                        'K':5,
                        'M':100,
                        'Noise_Cov':20,
                        'State_Dim':2,
                        'Control_Dim':2,
                        'Control_Cost':0.1,
                        'Algorithm':'MPPI'
                        },
    'DoubleSlit2D':{
                        'Task':'CrossSlit',
                        'N':50,
                        'K':200,
                        'M':100,
                        'Noise_Cov':50,
                        'State_Dim':2,
                        'Control_Dim':2,
                        'Control_Cost':0.001,
                        'Algorithm':'MPPI'
                        }
}
