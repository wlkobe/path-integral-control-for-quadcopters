import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cm
import pandas as pd
import numpy as np
from numpy.linalg import inv
import random
import quadcopter as QUAD
import gui as GUI

#from xD_MPPI_Modi_Normal import MPPI
from xD_MPPI_Modi import MPPI
from parameters import Env_Parameters
from logger import Logger


def Experiment(Parameters):

    Env = 'Quadcopter'#'DoubleSlit2D'
    #Env = 'DoubleSlit2D'
    mppi = MPPI(Env, Parameters[Env])
    Flag_GUI = Parameters[Env]['Flag_GUI']
    RUN = True
    epoch = 0
    step = 0

    control_output_sequence = np.array([np.zeros(mppi.env.CONTROL_DIM)])

    if Flag_GUI:
        if Env == 'Quadcopter':
            mppi.env.render('q1', mppi.env.STATE_INIT[0:3], mppi.env.STATE_INIT[6:9])
        else:
            mppi.env.render_init()

    logger = Logger()
    logger.enable_logger()
    logger.logging_initial(logger, Env, Parameters[Env])

    # Test Sampling Trajectory
    #while RUN:
    #    mppi.sampling_trajectories()

    while RUN:
        if Env == 'Quadcopter':
            if Parameters[Env]['Flag_Wind']:
                mppi.env.quad.Flag_Wind = Parameters[Env]['Flag_ModelWind']
            else:
                mppi.env.quad.Flag_Wind = False
        mppi.sampling_trajectories()
        if Env == 'Quadcopter':
            mppi.env.quad.Flag_Wind = Parameters[Env]['Flag_Wind']
        #logger.logging_sampling(logger, mppi)
        mppi.control_update()
        control_output, control_sequence, state_init = mppi.sequence_update()
        logger.logging_update(logger, control_output, control_sequence, state_init)

        # Plot the control sequence every epoch
        if Flag_GUI:
            coolwarm = plt.get_cmap('coolwarm')
            my_norm = colors.Normalize(0, 1000)
            map = cm.ScalarMappable(norm=my_norm, cmap='coolwarm')

            x = np.array([np.full(mppi.env.STATE_DIM, None) for _ in range(mppi.N)])
            # There is problem, because x[0] should be one step before the state_init
            x[0] = state_init
            for i in range(len(control_sequence)-1):
                x[i+1], _, _, temp_collision = mppi.env.step(x[i], control_sequence[i], np.zeros(mppi.env.CONTROL_DIM))
                if temp_collision:
                    break
                #mppi.env.render('q1', x[i+1][0:3], x[i+1][6:9])
            if Env == 'Quadcopter':
                print('')
                #mppi.env.plot_trajectory(x[:, 0:3], 'r')
            else:
                _, temp_cost, _= mppi.env.cost_path_calculate(x[0], control_output)
                my_col = map.to_rgba(np.clip(temp_cost, 0, 1000))
                mppi.env.ax1.plot(x[:,0], x[:,1], alpha = 0.1, c = my_col)
                print('Epoch: {}, Step: {}, Cost: {}'.format(epoch, step, temp_cost))

        # Update the rendering of quadcopter
        if Flag_GUI and Env == 'Quadcopter':
            # Check if quadcopter collision the ring
            if mppi.env.check_collision(state_init):
                mppi.env.gui.color_ring = 'r'
            mppi.env.render('q1', state_init[0:3], state_init[6:9])

        # Print imformations every step
        v = np.sqrt(state_init[3]**2 + state_init[4]**2 + state_init[5]**2)
        distance = np.sqrt((state_init[:3] - mppi.env.GOAL[:3]).dot(state_init[:3] - mppi.env.GOAL[:3]))
        print(' Epoch: {} \n Step: {} \n Control Output: {} \n State Init: {} \n Path Cost: {} \n Velocity: {}, Distance: {}'.format(epoch, step, control_output, state_init, mppi.env.cost_path_calculate(state_init, control_output), v, distance))
        #print(' Seperate Cost: {}, {}, {}, {}'.format(200*((state_init[0]/(2.0/2))**2+(state_init[1]/(2.0/2))**2-1)**2, 20*(state_init[2]-2)**2, 20*(v - 1.5)**2, 0.5*(state_init[3:]).dot(state_init[3:])))

        flag_finished, state_sequence, control_sequence, cost_sequence, flag_success = mppi.env.model_update(control_output)
        if flag_finished:
            print('Epoch Finished')
            if Env != 'Quadcopter':
                mppi.env.plot_trajectory(2, state_sequence[:,0], state_sequence[:,1])
            # Initial the values for the next epoch
            mppi.STATE_INIT = mppi.env.STATE_INIT
            mppi.state_sequence = np.array([np.full(mppi.env.STATE_DIM, None) for _ in range(mppi.N)])
            mppi.control_sequence = np.zeros((mppi.N, mppi.env.CONTROL_DIM))
            logger.logging_final(logger, flag_success, state_sequence, control_sequence, cost_sequence)
            logger.file_save(Env, Parameters[Env]['Task'], Parameters[Env]['Num'])
            #mppi.env.gui.save_figure(Env, Parameters[Env]['Task'], logger.Time, epoch)
            if epoch >= 10:
                RUN = False

        # Update Epoch & Step
        step += 1
        if flag_finished:
            epoch += 1
            step = 0
        logger.update_step(epoch, step)

if __name__ == '__main__':

    df = pd.read_csv('Experiments.csv')
    df_experiments = df[(df['Task'] == 'Ring') & (np.isnan(df['Finished']))]
    df_experiments = df_experiments[(df_experiments['Round'] == 4) & (df_experiments['Machine'] == 62)]

    for n in range(len(df_experiments)):
        Env_Parameters = {
            'Quadcopter':{
                                'Num':df_experiments.iloc[n].Number,
                                'Flag_GUI':False,
                                'Flag_Wind':df_experiments.iloc[n].Wind,
                                'Flag_ModelWind':df_experiments.iloc[n].ModelWind,
                                'Flag_CovarianceVariable':df_experiments.iloc[n].CovarianceVariable,
                                'Flag_NegativeThrust': df_experiments.iloc[n].NegativeThrust,
                                'Task':df_experiments.iloc[n].Task,#'Goal',
                                #'Task':'Ring',
                                #'Task':'Path',
                                'N':df_experiments.iloc[n].N,#30,
                                'K':df_experiments.iloc[n].K,#100,
                                'M':df_experiments.iloc[n].M,#150,
                                #'Noise_Cov':np.array([0.5,0.5,0.5,10]),
                                'Noise_Cov':np.array([df_experiments.iloc[n].Noise_Angle,
                                                        df_experiments.iloc[n].Noise_Angle,
                                                        df_experiments.iloc[n].Noise_Angle,
                                                        df_experiments.iloc[n].Noise_Thrust]),#np.array([1,1,1,40]),
                                'State_Dim':12,
                                'Control_Dim':4,
                                'Control_Cost':df_experiments.iloc[n].R,
                                'Algorithm':'MPPI',
                                #'Algorithm':'Info_MPPI',
                                'Wind_Para':{'mean_speed': [-10, 0, 0], 'gradient_height': 100},
                                'QUADCOPTER':{'q1':{'position':[-1.5, -1.5, 0.0],'orientation':[0,0,0],'L':0.3,'r':0.1,'prop_size':[10,4.5],'weight':1.2}},
                                'GOAL':{'q1':{'position':[1.5,1.5,4.0], 'orientation':[0,0,0]}}
                        }}
        print('Num: {}, N: {}, K: {}, M: {}, R: {}, Noise: {}, ModelWind: {}, CovarianceVariable: {}'.format(Env_Parameters['Quadcopter']['Num'],
                                                                                                        Env_Parameters['Quadcopter']['N'],
                                                                                                        Env_Parameters['Quadcopter']['K'],
                                                                                                        Env_Parameters['Quadcopter']['M'],
                                                                                                        Env_Parameters['Quadcopter']['Control_Cost'],
                                                                                                        Env_Parameters['Quadcopter']['Noise_Cov'],
                                                                                                        Env_Parameters['Quadcopter']['Flag_ModelWind'],
                                                                                                        Env_Parameters['Quadcopter']['Flag_CovarianceVariable']))
        Experiment(Env_Parameters)
