import numpy as np
import pandas as pd
import datetime as datetime

class Logger():
    def __init__(self):
        self.name_columns = ['Epoch', 'Step', 'Phase', 'Parameter', 'Type', 'Value']
        self.name_phases = ['Initial', 'Sampling', 'Update', 'Final']
        self.name_parameters = {
                            'Initial': ['Env', 'Task', 'N', 'K', 'M', 'Noise_Cov', 'State_Dim', 'Control_Dim', 'Control_Cost', 'Temperature', 'Algorithm'],
                            'Sampling': ['Control_Seq', 'Noise_Mat', 'State_Mat', 'Cost_Mat', 'Delta_U_Mat', 'Cost_Total'],
                            'Update': ['Control_Out', 'Control_Seq', 'State_Init'],
                            'Final': ['Flag_Success', 'Control_Seq', 'State_Seq', 'Cost_Seq']
        }
        self.name_type = ['Bood', 'String', 'Int', 'Double', 'Array']

        self.Flag_Enable = False

        self.df = pd.DataFrame(columns = ['Epoch', 'Step', 'Phase', 'Parameter', 'Type', 'Value'])
        self.Time = datetime.datetime.now().strftime("%Y%m%d%H%M%S")

        self.epoch = 0
        self.step = 0

    def update_step(self, epoch, step):
        self.epoch = epoch
        self.step = step

    # Enable the Flag of Logger
    def enable_logger(self):
        self.Flag_Enable = True

    # Disable the Flag of Logger
    def disable_logger(self):
        self.Flag_Enable = False

    # Check if the Dataframe has the right form
    def check_form(self, dict):
        for column in self.name_columns:
            if dict.__contains__(column):
                if column == 'Phase' and dict[column] not in self.name_phases:
                    print('Wrong Type: The Name of Phase: <{}> is not correct!'.format(dict[column]))
                    return False
                elif column == 'Parameter':
                    if dict[column] not in self.name_parameters[dict['Phase']]:
                        print('Wrong Type: The Name of Parameter: <{}> is not correct!'.format(dict[column]))
                        return False
            else:
                print('Wrong Type: The Name of Column: <{}> is not correct!'.format(column))
                return False
        #print('Type Check Passed!')
        return True

    # Logging the Datas in the Dataframe
    def logging(self, phase, parameter, type, value):
        # Update the value of DataFrame
        dict = {'Epoch': self.epoch,
                'Step': self.step,
                'Phase': phase,
                'Parameter': parameter,
                'Type': type,
                'Value': value}
        if self.check_form(dict):
            self.df = self.df.append(dict, ignore_index=True)
            return True
        return False

    def logging_initial(self, logger, Env, Parameters):
        #'Initial': ['Env', 'Task', 'N', 'K', 'M', 'Noise_Cov', 'State_Dim', 'Control_Dim', 'Control_Cost', 'Temperature', 'Algorithm']
        logger.logging(phase='Initial', parameter='Env', type='String', value=Env)
        logger.logging(phase='Initial', parameter='Task', type='String', value=Parameters['Task'])
        logger.logging(phase='Initial', parameter='N', type='Int', value=Parameters['N'])
        logger.logging(phase='Initial', parameter='K', type='Int', value=Parameters['K'])
        logger.logging(phase='Initial', parameter='M', type='Int', value=Parameters['M'])
        logger.logging(phase='Initial', parameter='Noise_Cov', type='Double', value=Parameters['Noise_Cov'])
        logger.logging(phase='Initial', parameter='State_Dim', type='Int', value=Parameters['State_Dim'])
        logger.logging(phase='Initial', parameter='Control_Dim', type='Int', value=Parameters['Control_Dim'])
        logger.logging(phase='Initial', parameter='Control_Cost', type='Double', value=Parameters['Control_Cost'])
        logger.logging(phase='Initial', parameter='Temperature', type='Double', value=Parameters['Noise_Cov']*Parameters['Control_Cost'])
        logger.logging(phase='Initial', parameter='Algorithm', type='String', value=Parameters['Algorithm'])

    def logging_sampling(self, logger, mppi):
        #'Sampling': ['Control_Seq', 'Noise_Mat', 'State_Mat', 'Cost_Mat', 'Delta_U_Mat', 'Cost_Total']
        logger.logging(phase='Sampling', parameter='Control_Seq', type='Array', value=mppi.control_sequence)
        logger.logging(phase='Sampling', parameter='Noise_Mat', type='Array', value=mppi.control_noise_all)
        logger.logging(phase='Sampling', parameter='State_Mat', type='Array', value=mppi.state_sequence_all)
        logger.logging(phase='Sampling', parameter='Cost_Mat', type='Array', value=mppi.cost_matrix)
        logger.logging(phase='Sampling', parameter='Delta_U_Mat', type='Array', value=mppi.delta_u_matrix)
        logger.logging(phase='Sampling', parameter='Cost_Total', type='Array', value=mppi.cost_total)

    def logging_update(self, logger, control_output, control_sequence, state_init):
        #'Update': ['Control_Out', 'Control_Seq', 'State_Init']
        logger.logging(phase='Update', parameter='Control_Out', type='Array', value=control_output)
        logger.logging(phase='Update', parameter='Control_Seq', type='Array', value=control_sequence)
        logger.logging(phase='Update', parameter='State_Init', type='Array', value=state_init)

    def logging_final(self, logger, Flag_Success, State_Seq, Control_Seq, Cost_Seq):
        #'Final': ['Flag_Success', 'Control_Seq', 'State_Seq', 'Cost_Seq']
        logger.logging(phase='Final', parameter='Flag_Success', type='Bool', value=Flag_Success)
        logger.logging(phase='Final', parameter='Control_Seq', type='Array', value=Control_Seq)
        logger.logging(phase='Final', parameter='State_Seq', type='Array', value=State_Seq)
        logger.logging(phase='Final', parameter='Cost_Seq', type='Array', value=Cost_Seq)

    # Read the File of Logger
    def file_read(self, filename):
        if self.Flag_Enable:
            temp_df = pd.read_csv(filename)
            if (temp_df.columns == self.name_columns).all():
                self.df = temp_df
                return True
        return False

    # Save the File of Logger
    def file_save(self, Env, Task, num, filename = 'Logger/{}_Logger_{}_{}_{}.csv'):
        if self.Flag_Enable:
            if (self.df.columns == self.name_columns).all():
                self.df.to_csv(filename.format(num, Env, Task, self.Time), index = False)
                return True
        return False
